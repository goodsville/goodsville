2020.02.21.

Process flow will be described by the UseCase diagram.

We will have the following pages:
1. Home page (index)
2. User Registration page
3. Login page
4. User profile page
5. Products page
6. Contacts page - static page
7. FAQ page - static page
8. Cart page
9. Purchase finalization page (from Cart)
10. Purchase details confirmation page (from finalization)ű

We will have the following Servlets:
1. Product Servlet
2. User Servlet
3. Cart Servlet
4. ??? PurchaseOrder Servlet ???

Every Servlet will have only 2 methods used: doGet() and doPost()

Using the following Services:
1. Product Service
	-filters and shows (lists) filtered product list
	-adds new product to the stock/store (new record of product)
	-deletes product from stock/store (delete record of product)
	-checks if given product is available for purchase (on stock)
	
2. Cart Service
	-lists products in itself (in cart instance)
	-deletes product from its own product list
	-summarizes its purchase value: sum of its contained products' price (payable sum)
	-summarizes its purchase quantity: counts number of products (number of items in cart)
	-finishing purchase: adds to purchase table (backend) and removes from products table the product or decreases its quantity (if was not 1)
	
3. User Service
	-adds user to User table
	-deletes user from User table
	-modifies user in User table
	

DataBase, Entities and DTOs:
We will keep the following rule: 1 record = 1 Entity = 1 DTO (will not yet create combined DTOs (from different Entities)).

DataBase name: goodsville_db
Tables:
1. User
2. Password
3. Product
4. Cart			 //(was "Purchase", we renamed to "Cart" in db)
5. Category
6. Sub_category

The table relations is described in the Entity Relationship diagram.


Storing pictures for the goods (products): everyone on local drive outside of the versioned project folder -- not to transfer and burden GitLab!!!
When user will upload picture for the product, the URL (path) of the image will be stored in database table.
--------------------------------------------------------------
2020.02.28
--------------------------------------------------------------

0. we keep the Issue description structure: 1. What 2. Why 3.Test
	What we are doing in this issue.
	Why we are doing it.
	How we will test function/operation of the unit done.


1. we modify the type of mobile (phone) number in user table from varchar(45) to int
2. we modify the table ids' names from single "id" to "tablename_id"
3. we modify the foreign key references according to point
4. we modify the tables cart and cart_products (as we redisign Cart handling from db-dependent to have data sotred in Session (not in db))
	cart to rename to purchase
	cart_products to purchase_history
5. change concept of identification of users' roles to:
	3 tables
		user table:
		role table: role_id, name
		id_users table (switching table): role_id, user_id
6. add product_price field to purchase_history for storing the product's price at the time of purchase (the price for which it was
	purchased)


7. we set up the test data with SQL script in the database- everyone for herself/himself - as the db is not on server but on localhost
8. we all use the jdbc/goodsville JNDI for the db in Payara Resource to be able to use the same project for the localhost db connection
9. 

Next steps:
Gabi sets up the peristence.xml with JTA, JNDI: jdbc/goodsville
Gabi generates the entities
Barbi codes the AbstractItemServices class
Barbi codes one ProductServices
István sets up logback
Barbi sets in the lombok dependency
István codes the Servlet

András codes the DTO
András codes the Dao


1 Dao: látja az összes entity-t
1 Dto: veszi össze 


András sets up the check-style - final setup
András sets up the spotBugs
----------------

2020-03-04


Session timeout 20 (Config)
Jacoco 0.01 coverage (Config)
Mockito dependency (Config)
FAQ page (JSP)
Products Datatable (Frontend)	| 
HomePage Servlet + FAQ Servlet	| because WEB-INF JSPs are only accessible via Servlets
UserProfile JSP
UserProfile Servlet
UserService
UserDto
UserDao

Rename dao.save() to dao.create()
Test getProducts() | list empty, null, one product, multiple product

If surplus time:
UserRegistration JSP

----------------------------------------------------------------------------------------------------------------------------
DAO tesztelés: getProductByUser - ez már üzleti logika, mert adsz át paramétert.
----------------------------------------------------------------------------------------------------------------------------
Visszaflow:
Felhasználó form-on ad paramétereket új product létrehozására - ezt átveszi a service, kreál dto-t, a dto-t átadja a dao-nak
dao entity-ket csinál a dto-kból és persist-álja.
----------------------------------------------------------------------------------------------------------------------------

2020-03-08
rekord inaktiválás (törlés helyett): plusz egy oszlop hogy aktív / inaktív
és az összes SELECT-et ki kell egészíteni azzal, hogy csak az aktív rekordok közül válogasson le
--------------------------------------------------------------




---
Product table Db aktív oszlop felvétele					| Barbi
Product entity -"-										| Barbi
Product dto -"-											| Barbi
Filterek módosítása az aktív oszlop hozzáadaásával		| Barbi
Products.jsp szűrő felület bal oldalra felvinni 		| Barbi
Szűrés-be: userId szerinti szűrés					    | Barbi
Tesztelés:												| Barbi 
----
Login felület felugró ablak								| Gabi
Frontend a newProduct form-ra							| Gabi
Entity-k: Category-kat összekötni a Sub-category-kkal   | Gabi
 kapcsolat
Category-t a flow-n végigvinni: új dao service-szel ami
 a subcategory-kat és a category-kat hívja le
ezt átküldeni servelet-en keresztül a newProduct.jsp-re |Gabi
Product details oldal									|István
Product details: update funkció (termék módosítás)
New products validálás back-end:						|Gabi
Tesztelés												|Gabi
Tesztelés												|István


User profile: my products szekció 	| András
User módosítás:						| András
User regisztráció:  				| András
Session a cart-nak: 				| András
Tesztelések 						| András

UserProfile Servlet 				| András
UserService 						| András
UserDto 							| András
UserDao 							| András
User-nek kell egy productlist 		| András

Crud repo módosításait mindenki találja ki.


response