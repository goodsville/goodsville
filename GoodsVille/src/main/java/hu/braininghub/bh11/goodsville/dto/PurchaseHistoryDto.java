package hu.braininghub.bh11.goodsville.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//  @author Istvan Palovics
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class PurchaseHistoryDto {

    @EqualsAndHashCode.Include
    private Integer id;

    private UserDto userDto;

    private ProductDto productDto;

    private Integer price;
}
