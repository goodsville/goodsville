package hu.braininghub.bh11.goodsville.dto;

import java.time.LocalDate;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//  @author Istvan Palovics
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class PurchaseDto {

    @EqualsAndHashCode.Include
    private Integer id;

    private UserDto userDto;

    private LocalDate date;
    
    public PurchaseDto(Integer purchaseId) {
        this.id = purchaseId;
    }
}
