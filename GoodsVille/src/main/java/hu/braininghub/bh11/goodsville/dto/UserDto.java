/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.goodsville.dto;

import java.io.Serializable;
import java.util.List;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author andra
 */
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class UserDto implements Serializable {
    @EqualsAndHashCode.Include
    private Integer id;
    private String userName;
    private Boolean isAdmin;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private Integer mobile;
    private String city;
    private String address;
    private List<ProductDto> ownProducts;

   

}
