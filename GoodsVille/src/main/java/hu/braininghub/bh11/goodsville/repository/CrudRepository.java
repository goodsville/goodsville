/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.goodsville.repository;

import java.util.Optional;

/**
 *
 * @author andra
 */
public interface CrudRepository<Entity, ID> {

    Iterable<Entity> findAll();

    void deleteById(ID id);

    void save(Entity entity);

    void update(Entity entity);

    Optional<Entity> findById(ID id);

    Iterable<Entity> findByName(String name);

    int count();
}
