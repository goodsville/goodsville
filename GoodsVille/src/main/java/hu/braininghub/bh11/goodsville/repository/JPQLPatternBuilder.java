package hu.braininghub.bh11.goodsville.repository;

import lombok.Data;

@Data
public class JPQLPatternBuilder {

    private String name;
    private Integer categoryId;
    private String subcategoryName;
    private String justWithImage; 
    private Integer priceMin;
    private Integer priceMax;
    private String searchInDescriptionToo;
    private Integer quality;
    private String city;
    private String userName;

    public JPQLPatternBuilder(String name, Integer categoryId,
            String subcategoryName, String justWithImage, String searchInDescriptionToo,
            Integer priceMin, Integer priceMax, Integer quality, String city, String userName) {
        this.name = name;
        this.categoryId = categoryId;
        this.subcategoryName = subcategoryName;
        this.justWithImage = justWithImage;
        this.priceMin = priceMin;
        this.priceMax = priceMax;
        this.searchInDescriptionToo = searchInDescriptionToo;

        this.quality = quality;
        this.city = city;
        this.userName = userName;
    }

    private StringBuilder addStringAttributeToQuery(StringBuilder sb, String query, String attribute) {
        if (attribute != null && !attribute.isEmpty()) {
            sb.append(" AND ");
            sb.append(query);
            sb.append(" LIKE ");
            sb.append("'%");
            sb.append(attribute);
            sb.append("%'");
        }
        return sb;
    }

    private StringBuilder addIntAttributeToQuery(StringBuilder sb, String query, Integer attribute) {
        if (attribute != null && attribute != 0) {
            sb.append(" AND ");
            sb.append(query);
            sb.append(" = ");
            sb.append(attribute);
        }
        return sb;
    }

    private StringBuilder addSearchInDescriptionTooAttributeToQuery(StringBuilder sb, String query, String attribute, String descriptionToo) {
        if (descriptionToo != null && !descriptionToo.isEmpty()) {
            if (attribute != null && !attribute.isEmpty()) {
                sb.append(" AND (p.name LIKE '%");
                sb.append(attribute);
                sb.append("%'");
                sb.append(" OR p.description LIKE '%");
                sb.append(attribute);
                sb.append("%')");
            }
        } else {
            sb = addStringAttributeToQuery(sb, query, attribute);
        }
        return sb;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT p FROM Product p WHERE p.active = 1");
        
        if(this.priceMin!=0){ 
            sb.append(" AND p.price > ");
            sb.append(priceMin);
        }
        
        if(this.priceMax!=0){
            sb.append(" AND p.price < ");
            sb.append(priceMax);
        }
            
        addSearchInDescriptionTooAttributeToQuery(sb, "p.name", name, searchInDescriptionToo);

        sb = addStringAttributeToQuery(sb, "p.subcategory.name", subcategoryName);
        sb = addStringAttributeToQuery(sb, "p.user.city", city);
        sb = addStringAttributeToQuery(sb, "p.user.userName", userName);

        sb = addIntAttributeToQuery(sb, "p.subcategory.category.id", categoryId);
        sb = addIntAttributeToQuery(sb, "p.quality", quality);

        if (this.justWithImage != null && !this.justWithImage.isEmpty()) {
            sb.append(" AND p.imagePath IS NOT NULL");
        }

        return sb.toString();
    }
}
