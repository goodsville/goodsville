package hu.braininghub.bh11.goodsville.repository;

import hu.braininghub.bh11.goodsville.dto.ProductDto;
import java.util.List;

/**
 *
 * @author gabri
 */

public interface ShoppingCart {

    public List<ProductDto> addToCart(List<ProductDto> list, ProductDto product);

    public List<ProductDto> removeFromCart(List<ProductDto> list, ProductDto product);

}
