/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.goodsville.dto.exceptions;

/**
 *
 * @author andra
 */
public class NoSuchUserException extends UserException {

    
    public NoSuchUserException() {
        super("No such user exists.");
    }

    public NoSuchUserException(String message) {
        super(message);
    }
    
    
}
