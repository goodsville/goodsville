/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.goodsville.dto.exceptions;

/**
 *
 * @author andra
 */
public class MultipleUsersFoundWhenSingleIsNeeded extends UserException{

    public MultipleUsersFoundWhenSingleIsNeeded(String message) {
        super(message);
    }
    
    
}
