package hu.braininghub.bh11.goodsville.dto.exceptions;


//  @author Istvan Palovics

public class NoSuchProductException extends RuntimeException{

    public NoSuchProductException() {
        super("There is no product with this Id");
    }    
    
}
