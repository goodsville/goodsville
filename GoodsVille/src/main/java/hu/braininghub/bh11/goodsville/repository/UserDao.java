/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.goodsville.repository;

import hu.braininghub.bh11.goodsville.dto.exceptions.NoSuchUserException;
import hu.braininghub.bh11.repository.entity.User;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andra
 */
@Singleton
@LocalBean
public class UserDao implements CrudRepository<User, Integer> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Iterable<User> findAll() {

        return em.createQuery("SELECT u FROM User u", User.class).getResultList();
    }

    @Override
    public void deleteById(Integer id) {
        em.createQuery("UPDATE u FROM User u SET active = 0 WHERE u.id = :pattern")
                .setParameter("pattern", id);

    }

    @Override
    public void save(User newUser) {

        em.persist(newUser);
    }

    @Override
    public void update(User user) {

        em.persist(user);
    }

    @Override
    public Optional<User> findById(Integer id) {
        return Optional.of((User) em.createQuery("SELECT u FROM User u WHERE u.id = :pattern")
                .setParameter("pattern", id).getSingleResult());
    }

    @Override
    public List<User> findByName(String firstAndOrLastName) {

        return em.createQuery("SELECT u FROM User u WHERE u.lastName LIKE :pattern OR WHERE u.firstName LIKE :pattern")
                .setParameter("pattern", "%" + firstAndOrLastName + "%")
                .getResultList();

    }

    public Optional<User> findByUserName(String username) {
        Optional<User> result = Optional.empty();
        try {
            result = Optional.of((User) em.createQuery("SELECT u FROM User u WHERE u.userName = :pattern")
                    .setParameter("pattern", username)
                    .getSingleResult());
        } catch (NoResultException ex) {
            throw new NoSuchUserException();
        }
        return result;
    }

    @Override
    public int count() {
        return (Integer) em.createQuery("SELECT COUNT(u) FROM User u").getSingleResult();
    }

}
