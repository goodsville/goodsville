/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.goodsville.repository;

import javax.ejb.Singleton;
import hu.braininghub.bh11.repository.entity.Product;
import hu.braininghub.bh11.repository.entity.Subcategory;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author andra
 */
@Singleton
@LocalBean
@Slf4j
public class ProductDao implements CrudRepository<Product, Integer> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Iterable<Product> findAll() {
        return em.createQuery("SELECT p FROM Product p", Product.class).getResultList();
    }

    public Iterable<Product> findAllActive() {
        return em.createQuery("SELECT p FROM Product p WHERE p.active=1", Product.class).getResultList();
    }

    public Iterable<Product> filter(String productName, Integer categoryId,
            String subcategory, String justWithImage, String searchInDescriptionToo,
            int priceMin, int priceMax, int quality, String city, String userName) {
        JPQLPatternBuilder jpql = new JPQLPatternBuilder(productName, categoryId,
                subcategory, justWithImage, searchInDescriptionToo, priceMin, priceMax, quality, city, userName);
        return em.createQuery(jpql.toString(), Product.class)
                .getResultList();
    }

    @Override
    public Iterable<Product> findByName(String name) {
        return em.createQuery("SELECT p FROM Product p WHERE p.name LIKE :pattern", Product.class)
                .setParameter("pattern", "%" + name + "%")
                .getResultList();
    }

    public Product findByIdInt(int id) {
        return em.createQuery("SELECT p FROM Product p WHERE p.id = :id", Product.class)
                .setParameter("id", id).getSingleResult();
    }

    @Override
    public void deleteById(Integer id) {

    }

    @Override
    public void save(Product entity) {
        em.persist(entity);
    }

    public void save(Product newProduct, String subcategory) {

        newProduct.setSubcategory(findSubcategoryByName(subcategory));
        newProduct.setActive(1);
        em.persist(newProduct);
    }

    private Subcategory findSubcategoryByName(String subcategory) {
        return em.createQuery("SELECT s FROM Subcategory s WHERE s.name = :subcategory", Subcategory.class)
                .setParameter("subcategory", subcategory).getSingleResult();
    }

    public void update(Product entity, String subcategory) {
        entity.setSubcategory(findSubcategoryByName(subcategory));
        em.merge(entity);
    }

    @Override
    public Optional<Product> findById(Integer id) {
        return Optional.of((Product)em.createQuery("SELECT p FROM Product p WHERE p.id = :id", Product.class)
                .setParameter("id", id).getSingleResult());          
    }

    @Override
    public int count() {
        return (Integer) em.createQuery("SELECT COUNT(p) FROM Product p").getSingleResult();
    }


    public void delete(Product deletingProduct) {
        em.merge(deletingProduct);
    }

    @Override
    public void update(Product entity) {
        em.merge(entity);
    }


}
