package hu.braininghub.bh11.goodsville.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

//  @author Istvan Palovics
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class SubcategoryDto {

    @EqualsAndHashCode.Include
    private Integer id;

    private String name;

    private CategoryDto categoryDto;
}
