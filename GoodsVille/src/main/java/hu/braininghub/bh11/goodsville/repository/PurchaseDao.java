package hu.braininghub.bh11.goodsville.repository;

import hu.braininghub.bh11.repository.entity.Purchase;
import hu.braininghub.bh11.repository.entity.PurchaseHistory;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;

//  @author Istvan Palovics
@Singleton
@LocalBean
@Slf4j
public class PurchaseDao {

    @PersistenceContext
    private EntityManager em;

    public Purchase save(Purchase purchase) {
        em.persist(purchase);
//        em.refresh(purchase);
        return purchase;
    }

    public void save(PurchaseHistory purchaseHistory) {
        em.persist(purchaseHistory);
    }

    public Purchase findById(Integer id) {
        return em.createQuery("SELECT p FROM Purchase p WHERE p.id = :id", Purchase.class)
                .setParameter("id", id).getSingleResult();
    }
}
