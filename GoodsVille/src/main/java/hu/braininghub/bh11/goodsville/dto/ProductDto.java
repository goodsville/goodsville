/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.goodsville.dto;

//import hu.braininghub.bh11.repository.entity.Subcategory;
import java.io.Serializable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author andra
 */
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class ProductDto implements Serializable {

    @EqualsAndHashCode.Include
    private Integer id;

    private UserDto userDto;
    private String name;
    private Integer price;
    private String description;
    private String imagePath;
    private Integer quantity;
    private SubcategoryDto subcategoryDto;
    private Integer active;


    public ProductDto(Integer productId) {
        this.id = productId;
    }

}
