/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.goodsville.repository;

import hu.braininghub.bh11.repository.entity.Category;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author gabri
 */
@Singleton
@LocalBean
public class CategoryDao implements CrudRepository<Category, Integer> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Iterable<Category> findAll() {
        return em.createQuery("SELECT c FROM Category c").getResultList();
    }

    @Override
    public void deleteById(Integer id) {

        Category c = em.find(Category.class, id);
        if (c != null) {
            em.remove(c);
        }
    }

    @Override
    public void save(Category entity) {
        em.persist(entity);
    }

    @Override
    public void update(Category entity) {
        em.merge(entity);
    }

    @Override
    public Optional<Category> findById(Integer id) {

        try {
            Category c = (Category) em.createQuery("SELECT c FROM Category c WHERE c.id= :id")
                    .setParameter("id", id)
                    .getSingleResult();
            return Optional.of(c);
        } catch (NoResultException e) {
            return Optional.empty();
        }

    }

    @Override
    public Iterable<Category> findByName(String name) {
        return em.createQuery("SELECT c FROM Category c WHERE c.name LIKE :pattern")
                .setParameter("pattern", "%" + name + "%")
                .getResultList();
    }

    @Override
    public int count() {
        return em.createQuery("SELECT c FROM Category c").getResultList().size();

    }

}
