package hu.braininghub.bh11.goodsville.dto;

import java.io.Serializable;
import java.util.List;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author gabri
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class CategoryDto implements Serializable {

    @EqualsAndHashCode.Include
    private Integer id;

    private String name;
    private List<SubcategoryDto> subcategoryDto;

}
