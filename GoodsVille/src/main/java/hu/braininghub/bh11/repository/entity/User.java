/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.repository.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 *
 * @author andra
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "user")
public class User implements Serializable {
    
    @EqualsAndHashCode.Include
    @Id
    //@GeneratedValue(strategy =GenerationType.AUTO)
    @Column(name = "user_id")
    private Integer id;

    @Column(name = "username")
    private String userName;

    @Column(name = "admin")
    private Boolean isAdmin;

    @Column(name = "password")
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "mobile")
    private Integer mobile;

    @Column(name = "city")
    private String city;

    @Column(name = "address")
    private String address;

    @Column(name = "active")
    private Boolean isActive;
    
//    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    private List<Product> products;

    public User() {
    }

    private User(UserBuilder userBuilder) {
        this.address = userBuilder.address;
        this.city = userBuilder.city;
        this.email = userBuilder.email;
        this.firstName = userBuilder.firstName;
        this.isAdmin = userBuilder.isAdmin;
        this.lastName = userBuilder.lastName;
        this.mobile = userBuilder.mobile;
        this.password = userBuilder.password;
        this.userName = userBuilder.userName;

    }

    public static class UserBuilder {

        private Integer id;
        private String userName;
        private Boolean isAdmin;
        private String password;
        private String firstName;
        private String lastName;
        private String email;
        private Integer mobile;
        private String city;
        private String address;

        public UserBuilder(String userName, String password, String email) {
            this.userName = userName;
            this.password = password;
            this.email = email;

        }

        public UserBuilder isAdmin(Boolean isAdmin) {
            this.isAdmin = isAdmin;
            return this;
        }

        public UserBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public UserBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public UserBuilder mobile(Integer mobile) {
            this.mobile = mobile;
            return this;
        }

        public UserBuilder city(String city) {
            this.city = city;
            return this;
        }

        public UserBuilder address(String address) {
            this.address = address;
            return this;
        }

        public User build() {
            return new User(this);
        }

    }

    

}
