/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.model.servlet;

import hu.braininghub.bh11.goodsville.dto.ProductDto;
import hu.braininghub.bh11.goodsville.dto.UserDto;
import hu.braininghub.bh11.model.services.UserService;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author gabri
 */
@Slf4j
@WebServlet(name = "FirstServlet", urlPatterns = {"/FirstServlet"})
public class FirstServlet extends HttpServlet {

    @Inject
    private UserService userService;

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, ServletException, IOException {
        if (request.getSession().getAttribute("cartItems") == null) {
            request.getSession().setAttribute("cartItems", new ArrayList<ProductDto>());
            log.info("Empty Arraylist has benn created");

        }
        if (request.getRemoteUser() != null && request.getSession().getAttribute("currentUserInSession") == null) {
            UserDto userDto = new UserDto();
            try {
                userDto = userService.getUserByUserName(request.getRemoteUser());
                request.getSession().setAttribute("currentUserInSession", userDto);
                log.info("First servlet current user in session = " + request.getSession().getAttribute("currentUserInSession"));
            } catch (IllegalAccessException | InvocationTargetException ex) {
                Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }

        super.service(request, response);
    }
}
