/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.model.servlet;

import hu.braininghub.bh11.goodsville.dto.ProductDto;
import hu.braininghub.bh11.model.services.CartService;
import hu.braininghub.bh11.model.services.ProductService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "CartServlet", urlPatterns = {"/cart"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"admin", "user"}))
public class CartServlet extends FirstServlet {

    @Inject
    ProductService productService;

    @Inject
    CartService cartService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("/WEB-INF/cart.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int productId = Integer.parseInt(request.getParameter("id"));
        ProductDto product = productService.getProductById(productId);
        if (!product.getUserDto().getUserName().equals(request.getRemoteUser())) {
            List<ProductDto> cartItems = cartService.addToCart((ArrayList<ProductDto>) request.getSession().getAttribute("cartItems"), product);
            request.getSession().setAttribute("cartItems", cartItems);
        }

        response.sendRedirect("/GoodsVille-1.0-SNAPSHOT/products");

    }

}
