package hu.braininghub.bh11.model.services;

import hu.braininghub.bh11.goodsville.dto.ProductDto;
import hu.braininghub.bh11.goodsville.dto.PurchaseDto;
import hu.braininghub.bh11.goodsville.dto.UserDto;
import hu.braininghub.bh11.goodsville.repository.PurchaseDao;
import hu.braininghub.bh11.repository.entity.Product;
import hu.braininghub.bh11.repository.entity.Purchase;
import hu.braininghub.bh11.repository.entity.PurchaseHistory;
import hu.braininghub.bh11.repository.entity.User;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;

//  @author Istvan Palovics
@Slf4j
@Singleton
@LocalBean
@Setter
public class PurchaseService {

    @Inject
    private PurchaseDao purchaseDao;

    public void createNewPurchase(UserDto userDto, List<ProductDto> cart) {
        PurchaseDto purchaseDto = new PurchaseDto();
        LocalDate date = LocalDate.now();
        purchaseDto.setDate(date);
        Purchase purchase = new Purchase();
        User user = new User();
        try {
            BeanUtils.copyProperties(user, userDto);
            BeanUtils.copyProperties(purchase, purchaseDto);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(PurchaseService.class.getName()).log(Level.SEVERE, null, ex);
        }
        purchase.setUser(user);
        purchase = purchaseDao.save(purchase);
        createNewPurchaseHistory(purchase, cart);
    }
    
    private void createNewPurchaseHistory(Purchase purchase, List<ProductDto> cart) {
        for (ProductDto productDto : cart) {
            PurchaseHistory purchaseHistory = new PurchaseHistory();
            purchaseHistory.setPurchase(purchase);
            Product product = new Product();
            try {
                BeanUtils.copyProperties(product, productDto);
            } catch (IllegalAccessException | InvocationTargetException ex) {
                Logger.getLogger(PurchaseService.class.getName()).log(Level.SEVERE, null, ex);
            }
            purchaseHistory.setProduct(product);
            purchaseHistory.setPrice(product.getPrice());
            purchaseHistory.setQuantity(product.getQuantity());
            purchaseDao.save(purchaseHistory);
        }
    }



}
