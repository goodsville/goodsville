/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.model.services;

import hu.braininghub.bh11.goodsville.dto.CategoryDto;
import hu.braininghub.bh11.goodsville.dto.ProductDto;
import hu.braininghub.bh11.goodsville.dto.SubcategoryDto;
import hu.braininghub.bh11.goodsville.dto.exceptions.NoSuchProductException;
import hu.braininghub.bh11.goodsville.dto.UserDto;
import javax.inject.Inject;
import javax.ejb.Singleton;
import hu.braininghub.bh11.goodsville.repository.ProductDao;
import hu.braininghub.bh11.repository.entity.Product;
import hu.braininghub.bh11.repository.entity.User;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author andra
 */
@Slf4j
@Singleton
@LocalBean
public class ProductService {

    public static final int INACTIVE = 0;

    @Inject
    private ProductDao dao;

    public List<ProductDto> filter(String productName, Integer categoryId,
            String subcategory, String justWithImage, String searchInDescriptionToo,
            int priceMin, int priceMax, int quality, String city, String userName) {

        List<ProductDto> result = new ArrayList<>();

        for (Product p : dao.filter(productName, categoryId, subcategory,
                justWithImage, searchInDescriptionToo, priceMin, priceMax, quality,
                city, userName)) {

            ProductDto dto = new ProductDto();

            try {
                BeanUtils.copyProperties(dto, p);
                result.add(dto);
            } catch (IllegalAccessException | InvocationTargetException ex) {
                Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);

            }
        }

        return result;
    }

    public List<ProductDto> getProducts() {
        List<ProductDto> result = new ArrayList<>();

        for (Product p : dao.findAll()) {
            ProductDto dto = new ProductDto();

            try {
                BeanUtils.copyProperties(dto, p);
                UserDto userDto = new UserDto();
                BeanUtils.copyProperties(userDto, p.getUser());
                dto.setUserDto(userDto);
                result.add(dto);
            } catch (IllegalAccessException | InvocationTargetException ex) {
                Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);

            }
        }
        return result;
    }

    public List<ProductDto> getActiveProducts() {
        List<ProductDto> result = new ArrayList<>();

        for (Product p : dao.findAllActive()) {
            ProductDto dto = new ProductDto();

            try {
                BeanUtils.copyProperties(dto, p);
                result.add(dto);
                UserDto userDto = new UserDto();
                BeanUtils.copyProperties(userDto, p.getUser());
                dto.setUserDto(userDto);
            } catch (IllegalAccessException | InvocationTargetException ex) {
                Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);

            }
        }
        return result;
    }

    public List<ProductDto> getProductsByName(String str) {
        List<ProductDto> result = new ArrayList<>();

        for (Product p : dao.findByName(str)) {
            ProductDto dto = new ProductDto();

            try {
                BeanUtils.copyProperties(dto, p);
                UserDto userDto = new UserDto();
                BeanUtils.copyProperties(userDto, p.getUser());
                dto.setUserDto(userDto);
                result.add(dto);
            } catch (IllegalAccessException | InvocationTargetException ex) {
                Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return result;
    }

    public ProductDto getProductById(int id) throws NoSuchProductException {
        Optional<Product> optionalProduct = Optional.ofNullable(dao.findById(id).orElseThrow(() -> {
            return new NoSuchProductException();
        }));
        Product product = optionalProduct.get();
        ProductDto productDto = new ProductDto();
        //if (product.isPresent()) {
        try {
            BeanUtils.copyProperties(productDto, product);
            UserDto userDto = new UserDto();
            BeanUtils.copyProperties(userDto, product.getUser());
            productDto.setUserDto(userDto);
            SubcategoryDto subcategoryDto = new SubcategoryDto();
            BeanUtils.copyProperties(subcategoryDto, product.getSubcategory());
            productDto.setSubcategoryDto(subcategoryDto);
            CategoryDto categoryDto = new CategoryDto();
            BeanUtils.copyProperties(categoryDto, product.getSubcategory().getCategory());
            productDto.getSubcategoryDto().setCategoryDto(categoryDto);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
            // }
        }
        return productDto;
    }

    public boolean createNewProduct(ProductDto newProductDto, String subcategory) {

        Product newProduct = new Product();
        User user = new User();
        try {
            BeanUtils.copyProperties(newProduct, newProductDto);
            BeanUtils.copyProperties(user, newProductDto.getUserDto());
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return false;
        }
        newProduct.setUser(user);
        dao.save(newProduct, subcategory);
        return true;
    }

    public void updateProduct(String productName, String subcategory, int price,
            String description, String image, int quantity, int id) throws NoSuchProductException {

        //megkeressük id alapján a módosítani kívánt terméket, majd beállítjuk az új paramétereket
        Product updatingProduct = dao.findById(id).orElseThrow(() -> {
            return new NoSuchProductException();
        });
        updatingProduct.setName(productName);
        updatingProduct.setPrice(price);
        updatingProduct.setDescription(description);
        updatingProduct.setImagePath(image);
        updatingProduct.setQuantity(quantity);

        dao.update(updatingProduct, subcategory);
    }

    public void purchaseProduct(List<ProductDto> cart) {

        for (ProductDto dto : cart) {
            int id = dto.getId();

            log.info("Dto quantity=" + dto.getQuantity());
            Product product = dao.findById(id).orElseThrow(() -> {
                return new NoSuchProductException();
            });
            if (product.getQuantity() > dto.getQuantity()) {
                product.setQuantity(product.getQuantity() - dto.getQuantity());
                dao.update(product);
                log.info("Update product quantity");
            } else {
                product.setActive(INACTIVE);
                dao.update(product);
                log.info("Update product to inactive");
            }
        }
    }

    public void deleteProduct(int id) {
        Product deletingProduct = dao.findById(id).orElseThrow(() -> {
            return new NoSuchProductException();
        });
        //a product törlése igazából egy update, az active mezőt állítjuk át

        deletingProduct.setActive(INACTIVE);
        dao.update(deletingProduct);

    }

    public ProductDao getDao() {
        return dao;
    }

    public void setDao(ProductDao dao) {
        this.dao = dao;
    }

}
