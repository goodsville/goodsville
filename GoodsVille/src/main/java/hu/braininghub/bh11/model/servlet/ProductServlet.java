/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.model.servlet;

import hu.braininghub.bh11.goodsville.dto.CategoryDto;
import javax.inject.Inject;
import hu.braininghub.bh11.goodsville.dto.ProductDto;
import hu.braininghub.bh11.model.services.CategoryService;
import hu.braininghub.bh11.model.services.ProductService;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andra
 */
@WebServlet(name = "ProductServlet", urlPatterns = {"/products"})
public class ProductServlet extends FirstServlet {

    @Inject
    private ProductService productService;

    @Inject
    private CategoryService categoryService;

    private Boolean message;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<ProductDto> productlist = productService.getActiveProducts();
        request.setAttribute("productlist", productlist);
        List<CategoryDto> categories = categoryService.getAllCategory();
        request.setAttribute("categories", categories);
        message = (Boolean) request.getSession().getAttribute("message");
        if (request.getSession().getAttribute("message") == null) {
            request.setAttribute("msg", "");
        }
        if (request.getSession().getAttribute("message") != null && message) {
            request.setAttribute("msg", "siker");
            request.getSession().setAttribute("message", null);
        }
        request.getRequestDispatcher("/WEB-INF/products.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int categoryId;
        int priceMin;
        int priceMax;
        int quality;

        String productName = request.getParameter("pname");

        String subcategory = request.getParameter("subcategory");
        String justWithImage = request.getParameter("justWithImage");
        String searchInDescriptionToo = request.getParameter("description");

        if ("none".equals(request.getParameter("category"))) {
            categoryId = 0;
        } else {
            categoryId = Integer.parseInt(request.getParameter("category"));
        }

        if (request.getParameter("priceMin").isEmpty()) {
            priceMin = 0;
        } else {
            priceMin = Integer.parseInt(request.getParameter("priceMin"));
        }

        if (request.getParameter("priceMax").isEmpty()) {
            priceMax = 0;
        } else {
            priceMax = Integer.parseInt(request.getParameter("priceMax"));
            if (priceMin > priceMax) {
                priceMax = 0;
            }
        }

        if (request.getParameter("quality").isEmpty()) {
            quality = 0;
        } else {
            quality = Integer.parseInt(request.getParameter("quality"));
        }

        String city = request.getParameter("city");
        String userName = request.getParameter("userName");

        List<ProductDto> productlist = productService.filter(productName,
                categoryId, subcategory, justWithImage, searchInDescriptionToo,
                priceMin, priceMax, quality, city, userName);

        message = (Boolean) request.getSession().getAttribute("message");

        List<CategoryDto> categories = categoryService.getAllCategory();
        request.setAttribute("categories", categories);
        
        request.setAttribute("productlist", productlist);
        request.getRequestDispatcher("/WEB-INF/products.jsp").forward(request, response);
    }

}
