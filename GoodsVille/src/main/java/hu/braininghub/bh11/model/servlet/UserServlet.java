/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.model.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import hu.braininghub.bh11.goodsville.dto.UserDto;
import hu.braininghub.bh11.model.services.UserService;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andra
 */
@WebServlet(name = "UserServlet", urlPatterns = {"/user"})
public class UserServlet extends FirstServlet {

    // private static final String USER_LOGGED_IN = "NO-ONE";
    @Inject
    UserService userService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserDto currentUser = (UserDto) request.getSession().getAttribute("currentUserInSession");

        ObjectMapper jsonMapper = new ObjectMapper();
        String userAsJSON = jsonMapper.writeValueAsString(currentUser);
        
      // request.setAttribute("userdetails", userAsJSON);
        response.getWriter().flush();

        response.getWriter().write(userAsJSON);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nameInRequest = request.getParameter("username");
        System.out.println("(Servlet) nameInRequest: " + nameInRequest);
        try {
            UserDto currentUser = userService.getUserByUserName(nameInRequest);
            request.setAttribute("currentUser", currentUser);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(UserServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(UserServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        request.getRequestDispatcher("/WEB-INF/userProfile.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
