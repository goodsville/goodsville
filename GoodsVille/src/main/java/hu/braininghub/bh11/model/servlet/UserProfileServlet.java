/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.model.servlet;

import hu.braininghub.bh11.goodsville.dto.UserDto;
import hu.braininghub.bh11.goodsville.dto.ProductDto;
import hu.braininghub.bh11.goodsville.dto.exceptions.NoSuchUserException;
import hu.braininghub.bh11.model.services.UserService;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author andra
 */
@Slf4j
@WebServlet(name = "userProfileServlet", urlPatterns = {"/userProfile"})
public class UserProfileServlet extends FirstServlet {

    @Inject
    UserService userService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            UserDto currentUser;
            currentUser = userService.getUserByUserName(request.getRemoteUser());
            log.info("RemoteUser = " + request.getRemoteUser());
            List<ProductDto> productlist = (List<ProductDto>) userService.getOwnProducts(currentUser);
            request.setAttribute("productlist", productlist);
            request.getRequestDispatcher("/WEB-INF/userProfile.jsp").forward(request, response);

        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(UserProfileServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        if ("put".equals((String) request.getAttribute("overrideMethod"))) {

            UserDto userToUpdate = null;
            try {
                userToUpdate = userService.getUserById(Integer.parseInt(request.getParameter("id")));
            } catch (IllegalAccessException ex) {
                request.setAttribute("errormessage", ex.getMessage());
            } catch (InvocationTargetException ex) {
                request.setAttribute("errormessage", ex.getMessage());
            } catch (NoSuchUserException ex) {
                request.setAttribute("errormessage", ex.getMessage());
            }
            if (userService.updateUser(userToUpdate)) {

                request.setAttribute("message", "Successfully updated");
            } else {
                request.setAttribute("message", "Update failed");
            }

            if (userToUpdate.equals((UserDto) request.getAttribute("currentUserInSession"))) {
                request.setAttribute("currentUserInSession", userToUpdate);
            }
            request.getRequestDispatcher("/WEB-INF/userProfile.jsp");
        } else {

            Boolean wasUpdateSuccessful = false;
            UserDto userDtoToUpdate = (UserDto) request.getSession().getAttribute("currentUserInSession");
            try {
                wasUpdateSuccessful = userService.updateUser(userDtoToUpdate);
            } catch (NoSuchUserException ex) {
                request.setAttribute("result", ex);
            }
            request.setAttribute("result", wasUpdateSuccessful);
            request.getRequestDispatcher("/WEB-INF/userProfile.jsp").forward(request, response);
        }
    }

}
