/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.model.services;

import hu.braininghub.bh11.goodsville.dto.CategoryDto;
import hu.braininghub.bh11.goodsville.dto.SubcategoryDto;
import hu.braininghub.bh11.goodsville.repository.CategoryDao;
import hu.braininghub.bh11.repository.entity.Category;
import hu.braininghub.bh11.repository.entity.Subcategory;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.Setter;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author gabri
 */
@Setter
@Singleton
@LocalBean
public class CategoryService {

    @Inject
    private CategoryDao dao;

    public List<CategoryDto> getAllCategory() {

        List<CategoryDto> result = new ArrayList<>();

        for (Category c : dao.findAll()) {
            CategoryDto dto = new CategoryDto();
            dto.setSubcategoryDto(new ArrayList<>());
            try {
                BeanUtils.copyProperties(dto, c);
                for (Subcategory s : c.getSubcategory()) {
                    SubcategoryDto sDto = new SubcategoryDto();
                    BeanUtils.copyProperties(sDto, s);
                    dto.getSubcategoryDto().add(sDto);
                }
                result.add(dto);
            } catch (IllegalAccessException | InvocationTargetException ex) {
                Logger.getLogger(CategoryService.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return result;
    }

}
