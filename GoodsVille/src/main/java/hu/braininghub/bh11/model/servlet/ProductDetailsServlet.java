/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.model.servlet;

import hu.braininghub.bh11.goodsville.dto.CategoryDto;
import hu.braininghub.bh11.goodsville.dto.ProductDto;
import hu.braininghub.bh11.model.services.CategoryService;
import hu.braininghub.bh11.model.services.ProductService;
import hu.braininghub.bh11.model.services.UserService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Szottyos
 */
@WebServlet(name = "ProductDetailsServlet", urlPatterns = {"/productDetails"})
public class ProductDetailsServlet extends FirstServlet {
    
    @Inject
    private ProductService productService;

    @Inject
    private UserService userService;

    @Inject
    private CategoryService categoryService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //átvesszük a termékhez tartozó id-t
        int id = Integer.parseInt(request.getParameter("id"));

        //megkeressük a terméket
        ProductDto productDto = productService.getProductById(id);
        List<CategoryDto> categories = categoryService.getAllCategory();
        request.setAttribute("categories", categories);
        request.setAttribute("product", productDto);
        request.getRequestDispatcher("/WEB-INF/productDetails.jsp")
                .forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //az action egy hidden field ami eldönti hogy update vagy delete történjen
        String action = request.getParameter("action");
        if ("update".equals(action)) {

            //átvesszük az új paramétereket
            String productName = request.getParameter("name");
            String category = request.getParameter("category");
            String subcategory = request.getParameter("subcategory");
            int price = Integer.parseInt(request.getParameter("price"));
            String description = request.getParameter("description");
            String image = request.getParameter("image");
            int quantity = Integer.parseInt(request.getParameter("quantity"));
            int id = Integer.parseInt(request.getParameter("id"));

            productService.updateProduct(productName, subcategory, price, description, image, quantity, id);

            //visszatöltjük a product oldalát már az új adatokkal
            doGet(request, response);
        }
        if ("delete".equals(action)) {
            //id alapján találjuk meg a törölni kívánt terméket
            int id = Integer.parseInt(request.getParameter("id"));
            productService.deleteProduct(id);

            //törlés után visszairányítjuk a usert a terméklistához
            response.sendRedirect("/GoodsVille-1.0-SNAPSHOT/products");
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
