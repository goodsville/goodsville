package hu.braininghub.bh11.model.services;

import hu.braininghub.bh11.goodsville.dto.ProductDto;
import hu.braininghub.bh11.goodsville.repository.ShoppingCart;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author gabri
 */
@Stateless
@LocalBean
public class CartService implements ShoppingCart {


    @Override
    public List<ProductDto> addToCart(List<ProductDto> list, ProductDto product) {
        if (product != null && !list.contains(product)) {
            list.add(product);
        }
        return list;
    }

    @Override
    public List<ProductDto> removeFromCart(List<ProductDto> list, ProductDto product) {
        if (product != null) {
            list.remove(product);
        }
        return list;
    }

}
