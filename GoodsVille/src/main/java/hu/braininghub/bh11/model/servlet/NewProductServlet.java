/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.model.servlet;

import hu.braininghub.bh11.goodsville.dto.CategoryDto;
import hu.braininghub.bh11.goodsville.dto.ProductDto;
import hu.braininghub.bh11.goodsville.dto.UserDto;
import hu.braininghub.bh11.model.services.CategoryService;
import hu.braininghub.bh11.model.services.ProductService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Szottyos
 */
@WebServlet(name = "newProductServlet", urlPatterns = {"/newProduct"})
public class NewProductServlet extends FirstServlet {

    @Inject
    private ProductService productService;
    
    @Inject
    private CategoryService categoryService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<CategoryDto> categories = categoryService.getAllCategory();
        request.setAttribute("categories", categories);
        request.getRequestDispatcher("/WEB-INF/newProduct.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String subcategory = request.getParameter("subcategory");
        
        ProductDto newProductDto = new ProductDto();
        newProductDto.setName(request.getParameter("productName"));
        newProductDto.setPrice(Integer.parseInt(request.getParameter("price")));
        newProductDto.setDescription(request.getParameter("description"));
        newProductDto.setImagePath(request.getParameter("image"));
        newProductDto.setQuantity(Integer.parseInt(request.getParameter("quantity")));
        newProductDto.setUserDto((UserDto) request.getSession().getAttribute("currentUserInSession"));
        boolean successfulCreating = productService.createNewProduct(newProductDto, subcategory);
        
        request.getSession().setAttribute("message", successfulCreating);        
        response.sendRedirect("/GoodsVille-1.0-SNAPSHOT/products");
    }
}
