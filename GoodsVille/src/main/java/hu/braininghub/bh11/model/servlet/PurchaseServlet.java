/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.model.servlet;

import hu.braininghub.bh11.goodsville.dto.ProductDto;
import hu.braininghub.bh11.goodsville.dto.UserDto;
import hu.braininghub.bh11.model.services.CartService;
import hu.braininghub.bh11.model.services.ProductService;
import hu.braininghub.bh11.model.services.PurchaseService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebServlet(name = "PurchaseServlet", urlPatterns = {"/purchase"})
public class PurchaseServlet extends FirstServlet {

    @Inject
    ProductService productService;

    @Inject
    CartService cartService;

    @Inject
    PurchaseService purchaseService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("/WEB-INF/purchase.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");
        if ("deleteFromCart".equals(action)) {
            int productId = Integer.parseInt(request.getParameter("id"));
            ProductDto product = productService.getProductById(productId);
            List<ProductDto> cartItems = cartService.removeFromCart((ArrayList<ProductDto>) request.getSession().getAttribute("cartItems"), product);
            request.getSession().setAttribute("cartItems", cartItems);

            response.sendRedirect("/GoodsVille-1.0-SNAPSHOT/cart");

        } else if ("finishPurchase".equals(action)) {
            List<ProductDto> cart = (List<ProductDto>) request.getSession().getAttribute("cartItems");

            for (ProductDto productDto : cart) {
                int quantity = Integer.parseInt(request.getParameter(productDto.getName()));
                productDto.setQuantity(quantity);
            }

            productService.purchaseProduct(cart);
            purchaseService.createNewPurchase((UserDto) request.getSession().getAttribute("currentUserInSession"), cart);
            request.setAttribute("history", cart);
            request.getSession().setAttribute("cartItems", new ArrayList<ProductDto>());
            request.getRequestDispatcher("/WEB-INF/purchase.jsp").forward(request, response);

        }

    }

}
