package hu.braininghub.bh11.model.servlet;

import hu.braininghub.bh11.goodsville.dto.ProductDto;
import hu.braininghub.bh11.model.services.ProductService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author gabri
 */
@Slf4j
@WebServlet(name = "CategoryFilterServlet", urlPatterns = {"/categoryFilter"})
public class CategoryFilterServlet extends HttpServlet {
    
    @Inject
    ProductService productService;
    
    private Boolean message;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int category = Integer.parseInt(request.getParameter("category"));
        
        log.info("Category name: " + category);
        
        List<ProductDto> productlist = productService.filter(null,
                category, null, null, null, 0, 0, 0, null, null);
        
        message = (Boolean) request.getSession().getAttribute("message");
        
        request.setAttribute("productlist", productlist);
        request.getRequestDispatcher("/WEB-INF/products.jsp").forward(request, response);
        
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
}
