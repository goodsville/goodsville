/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.model.servlet;

import hu.braininghub.bh11.goodsville.dto.UserDto;
import hu.braininghub.bh11.goodsville.dto.exceptions.NoSuchUserException;
import hu.braininghub.bh11.model.services.UserService;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author andra
 */
@Slf4j
@Singleton
@WebServlet(name = "UserDataControllerServlet", urlPatterns = {"/userDataControl"})
public class UserDataControllerServlet extends FirstServlet {

    @Inject
    UserService userService;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = request.getParameter("username");
        String password = request.getParameter("password");

        UserDto currentUser = null;
        MessageDigest encodedPassword;
        try {
            encodedPassword = MessageDigest.getInstance("SHA-256");

            byte[] encodedhash = encodedPassword.digest(
                    password.getBytes(StandardCharsets.UTF_8));

            currentUser = userService.getUserByUserName(userName);
            if (encodedPassword.equals(currentUser.getPassword())) {
                request.getSession().setAttribute("currentUserInSession", currentUser);
                request.getRequestDispatcher("/WEB-INF/userProfile.jsp").forward(request, response);
            } else {
                request.setAttribute("invalidPassword", "Provided password incorrect");
                request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
            }

        } catch (NoSuchAlgorithmException ex) {

            log.debug("password", ex.getMessage(), ex.getCause());
        } catch (IllegalAccessException ex) {
            log.info("password", ex.getMessage(), ex.getCause());
        } catch (InvocationTargetException ex) {
            log.info("password", ex.getMessage(), ex.getCause());
        } catch (NoSuchUserException ex) {
            log.info("password", ex.getMessage(), ex.getCause());
            request.setAttribute("invalidUser", userName);
            request.setAttribute("userException", userName + ": " + ex.getMessage());
            request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getAttribute("currentUserInSession") == null) {
            request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("/GoodsVille/userProfile.jsp").forward(request, response);
        }
    }

}
