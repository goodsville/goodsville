/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.model.servlet;
import hu.braininghub.bh11.model.services.UserService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author andra
 */
@Slf4j
@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends FirstServlet {

    @Inject
    UserService userService;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getRemoteUser() != null) {
            request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
        } else {
            response.sendRedirect("/GoodsVille-1.0-SNAPSHOT/userProfile");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getSession().invalidate();
        response.sendRedirect("/GoodsVille-1.0-SNAPSHOT/home");

    }

}
