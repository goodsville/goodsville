/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.model.services;

import hu.braininghub.bh11.goodsville.dto.ProductDto;
import hu.braininghub.bh11.goodsville.dto.UserDto;
import hu.braininghub.bh11.goodsville.dto.exceptions.NoSuchUserException;
import hu.braininghub.bh11.goodsville.repository.ProductDao;
import hu.braininghub.bh11.goodsville.repository.UserDao;
import hu.braininghub.bh11.repository.entity.Product;
import hu.braininghub.bh11.repository.entity.User;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author andra
 */
@Data
@Singleton
@LocalBean
@Slf4j
public class UserService {

    @Inject
    UserDao dao;
    
    @Inject
    ProductDao productDao;

    public List<UserDto> getUsers() {
        List<UserDto> result = new ArrayList<>();
        for (User u : dao.findAll()) {
            UserDto dto = new UserDto();
            try {
                BeanUtils.copyProperties(dto, u);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            }
            result.add(dto);
        }
        return result;
    }

    public UserDto getUserById(Integer userId) throws IllegalAccessException, InvocationTargetException, NoSuchUserException {
        UserDto userDto = new UserDto();

        User userEntity = dao.findById(userId).orElseThrow(NoSuchUserException::new);
        BeanUtils.copyProperties(userDto, userEntity);
        return userDto;

    }

    public UserDto getUserByUserName(String username) throws IllegalAccessException, InvocationTargetException {
        UserDto userDto = new UserDto();
        log.info("username= " + dao.findByUserName(username));
        User userEntity = dao.findByUserName(username).orElseThrow(NoSuchUserException::new);

        BeanUtils.copyProperties(userDto, userEntity);
        return userDto;

    }

    public Iterable<UserDto> getUsersByName(String name) {

        List<UserDto> result = new ArrayList<>();
        for (User u : dao.findByName(name)) {
            UserDto dto = new UserDto();
            try {

                BeanUtils.copyProperties(dto, u);
                result.add(dto);

            } catch (IllegalAccessException ex) {
                Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;

    }

    public boolean updateUser(UserDto userDto) throws NoSuchUserException {
        if (userDto == null) {
            throw new NoSuchUserException("was null");
        }
        boolean wasSuccessful = false;
        if (userDto.equals(dao.findById(userDto.getId()))) {
            try {
                User userToBeUpdated = dao.findById(userDto.getId()).orElseThrow(NoSuchUserException::new);

                BeanUtils.copyProperties(userToBeUpdated, userDto);

                dao.update(userToBeUpdated);

            } catch (IllegalAccessException ex) {
                Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            }
            wasSuccessful = true;
        }
        return wasSuccessful;
    }

    public void createNewUser(
            String userName, String password, String email,
            Boolean isAdmin, String firstName,
            String lastName, Integer mobil, String city, String address) {
        User newUser = new User.UserBuilder(userName, password, email)
                .isAdmin(isAdmin)
                .firstName(firstName)
                .lastName(lastName)
                .mobile(mobil)
                .city(city)
                .address(address)
                .build();

        dao.save(newUser);

    }

    public Iterable<ProductDto> getOwnProducts(UserDto userDto) {
        List<ProductDto> result = new ArrayList<>();
        
        List<Product> list = (List<Product>) productDao.filter(null, null, null, 
                null, null, 0, 0, 0, null, userDto.getUserName());
        
        for (Product p : list) {
            ProductDto dto = new ProductDto();
            try {
                BeanUtils.copyProperties(dto, p);
                result.add(dto);
            } catch (IllegalAccessException | InvocationTargetException ex) {
                Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);

            }
        }

        return result;
    }
}
