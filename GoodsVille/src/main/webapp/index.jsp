<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Start Page</title>
        <%@include file="menu/head.jsp"%>
        <link rel="stylesheet" type="text/css" href="style/circleCategMenu.css">
        <script src="script/circleMenuScript.js"></script>
        <script type="text/javascript">
            function setIntentionLogin() {
                var intentionForm = document.getElementById("loginForm");
                intentionForm.action = "/login";
                intentionForm.method = "get";
                intentionForm.innerHTML =
                        "<input type=\"hidden\" name=\"intention\" value=\"login\"></input>";
                intentionForm.submit();
            <c:set var="intention" scope="request" value="login"/>
            }

        </script>
        <style>
            .invisiblebutton {
                background: none;
                padding: 0px;
                border: none;
            }
            #icon {
                color: #0E449C;   
            }
        </style>
    </head>
    <body>
        <%@ include file="menu/menu.jsp" %>

        <c:set var="currentUser" scope="session" value='${sessionScope.currentUserInSession}'/>
        <div class="container p-5">
        </div>
        <div class="container">
            <h2 class="welcome">Welcome to GoodsVille,</h2> 
            <h4 class="welcome">wich is an online marketplace for Geeks where you can buy or sell everything related to videogames, PC-s or consoles.</h4>
        </div>

        <section class="iq-features">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-12"></div>
                    <div class="col-lg-6 col-md-12">
                        <div class="holderCircle">
                            <div class="logoplace">
                                <img src='<c:url value="/img/mainlogoWithoutbcg.svg" />'/>
                            </div>
                            <div class="round"></div>
                            <div class="dotCircle">
                                <form method="GET" action="categoryFilter">
                                    <span class="itemDot active itemDot1" data-tab="1">
                                        <button name="category" value="1237" type="submit" id="completed-task" class="invisiblebutton">
                                            <i class="fa fa-gamepad" id="icon"></i>
                                        </button>
                                        <span class="forActive"></span>
                                    </span>
                                    <span class="itemDot itemDot2" data-tab="2">
                                        <button name="category" value="1236" type="submit" id="completed-task" class="invisiblebutton">
                                            <i class="fab fa-xbox" id="icon"></i>
                                        </button>
                                        <span class="forActive"></span>
                                    </span>
                                    <span class="itemDot itemDot3" data-tab="3">
                                        <button name="category" value="1238" type="submit" id="completed-task" class="invisiblebutton">
                                            <i class="fa fa-keyboard-o" id="icon"></i>
                                        </button>
                                        <span class="forActive"></span>
                                    </span>
                                    <span class="itemDot itemDot4" data-tab="4">
                                        <button name="category" value="1235" type="submit" id="completed-task" class="invisiblebutton">
                                            <i class="fab fa-playstation" id="icon"></i>
                                        </button>
                                        <span class="forActive"></span>
                                    </span>
                                    <span class="itemDot itemDot5" data-tab="5"> 
                                        <button name="category" value="1239" type="submit" id="completed-task" class="invisiblebutton">
                                            <i class="fab fa-steam" id="icon"></i>
                                        </button>
                                        <span class="forActive"></span>
                                    </span>
                                    <span class="itemDot itemDot6" data-tab="6">
                                        <button name="category" value="1234" type="submit" id="completed-task" class="invisiblebutton">
                                            <i class="fas fa-laptop" id="icon"></i>
                                        </button>
                                        <span class="forActive"></span>
                                    </span>
                                </form>
                            </div>
                            <div class="contentCircle">
                                <div class="CirItem title-box active CirItem1">
                                    <h2 class="title"><span>Console</span></h2>
                                </div>
                                <div class="CirItem title-box CirItem2">
                                    <h2 class="title"><span>Xbox</span></h2>
                                </div>
                                <div class="CirItem title-box CirItem3">
                                    <h2 class="title"><span>Accessories</span></h2>
                                </div>
                                <div class="CirItem title-box CirItem4">
                                    <h2 class="title"><span>PS3 & PS4</span></h2>
                                </div>
                                <div class="CirItem title-box CirItem5">
                                    <h2 class="title"><span>Steam</span></h2>
                                </div>
                                <div class="CirItem title-box CirItem6">
                                    <h2 class="title"><span>PC</span></h2>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12"></div>
                </div>
            </div>
        </section>
        <%@include file="menu/footer.jsp"%>
    </body>
</html>
