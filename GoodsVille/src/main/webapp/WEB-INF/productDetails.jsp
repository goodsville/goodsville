<!DOCTYPE html>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
        <title>Product Details</title>
        <%@include file="/menu/head.jsp"%>
        <script>
            (function () {
                'use strict';
                window.addEventListener('load', function () {
                    var forms = document.getElementsByClassName('needs-validation');
                    var validation = Array.prototype.filter.call(forms, function (form) {
                        form.addEventListener('submit', function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
            function setDetailsHiddenAndModifyInputsVisible(ref) {
                // a Submit gomb neve funkci�j�hoz igazodva Save-re v�lt
                var buttonRef = ref;
                buttonRef.value = "Save";
                $('body').find('#modifyButton').html("Save")
                // a gomb Met�dusa �s t�pusa submitra v�lt
                buttonRef.setAttribute("onclick", "doSubmit()");
                buttonRef.setAttribute("type", "submit");
                // a product inform�ci�it elrejt�k
                var detailsRef = document.getElementById("details");
                detailsRef.hidden = true;
                // az �j adatokhoz tartoz� bevitel? mez?ket l�that�v� tessz�k
                var inputFieldsRef = document.getElementById("inputFields");
                inputFieldsRef.hidden = false;
                // megg�toljuk, hogy azonnal megh�v�djon a gomb �jonnan kapott
                //    doSubmit met�dusa 
                event.preventDefault();
            }

            function doSubmit() {
                // m�sodik gombnyom�sra t�rt�nik a servlet fel� t�rt�n? kommunik�ci�,
                //       ezt letiltottuk, �gy �jra enged�lyezni kell
                $this.unbind('submit').submit();
            }

            function askForConfirmation(ref) {
                var buttonRef = ref;
                var txt;
                var r = confirm("T�nyleg t�r�lni szeretn�d?");
                if (r === true) {
                    buttonRef.setAttribute("type", "submit");
                    $this.unbind('submit').submit();
                } else {
                    event.preventDefault();
                }
            }

            function checkUserIdAndAdmin() {
                var ownerId = "${sessionScope.currentUserInSession.id}";
                if (ownerId === "${product.userDto.id}" || "${sessionScope.currentUserInSession.isAdmin}" === "true") {
                    $("#deleteButton").show();
                    $("#modifyButton").show();
                    $("#addToCartButton").hide();
                } else {
                    $("#deleteButton").hide();
                    $("#modifyButton").hide();
                    $("#addToCartButton").show();
                }
            }
            function getSelectedCategory() {
                var c = document.getElementById("category");
                var categoryId = c.options[c.selectedIndex].value.toString();
                var subcategorySelect = document.getElementById("subcategory");
                subcategorySelect.innerHTML = "";
                subcategorySelect.insertAdjacentHTML('beforeend', "<option value=\"\">" + "Open this select menu" + " </option>");
            <c:forEach var="c" items="${categories}">
                var externalCatId = "${c.id}";
                <c:forEach var="s" items="${c.subcategoryDto}">
                if (categoryId === externalCatId) {
                    subcategorySelect.insertAdjacentHTML('beforeend', "<option value=\"" + "${s.name}" + "\"" + ">" + "${s.name}" + "</option>");
                }
                </c:forEach>
            </c:forEach>
            }
            ;
        </script>
    </head>


    <body onload="checkUserIdAndAdmin()">
        <%@ include file="../menu/menu.jsp" %>
        <div class="containter pt-3"></div>
        <div class="containter pt-3">

            <div class="container" id="details">
                <h1 class="my-4">
                    <small><c:out value="${product.name}"/></small>
                </h1>
                <div class="row">
                    <div class="col-md-5">
                        <img class="img-fluid" src="${product.imagePath}" height="500px">
                    </div>
                    <div class="col-md-5">
                        <h3 class="my-3">Product details</h3>
                        <ul>
                            <li>Seller: <c:out value="${product.userDto.userName}"/></li>
                            <li>Price: <c:out value="${product.price}"/> Ft</li>
                            <li>Quantity:  <c:out value="${product.quantity}"/></li>
                        </ul>
                        <div>
                            <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false"
                               aria-controls="multiCollapseExample1">Description</a>       
                            <div class="col">
                                <div class="collapse multi-collapse" id="multiCollapseExample1">
                                    <div class="card card-body">
                                        <c:out value="${product.description}"/>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <img src='<c:url value="/img/gameinglogoWithoutbcg.svg" />'/>
                    </div>
                </div>
            </div>

            <!-- a product adatait megjelen�tj�k az �tlag felhaszn�l�k sz�m�ra -->

            <div class="container pt-5">

                <div class="container">
                    <form class="needs-validation" method="post" action="productDetails" id="formOfProductDetailsModification"  novalidate>
                        <!-- a beviteli mez?k kezdetben rejtettek -->
                        <a id="inputFields" hidden="true">
                            <div class="form-group">
                                <div class="col-md-4 mb-3">
                                    <label for="productName">Product name</label>
                                    <input type="text" class="form-control" id="productName" name="name" value="${product.name}" required>
                                    <div class="invalid-feedback">Please provide a valid product name!</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 mb-3">
                                    <label for="category">Category</label>
                                    <select onchange="getSelectedCategory()" class="custom-select" id="category" name="category" required>
                                        <option hidden="true" id="${product.subcategoryDto.categoryDto.id}">${product.subcategoryDto.categoryDto.name}</option>
                                        <c:forEach var="c" items="${categories}">
                                            <option value="${c.id}"><c:out value="${c.name}"/></option>
                                        </c:forEach>
                                    </select>
                                    <div class="invalid-feedback">Example invalid custom select feedback</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 mb-3">
                                    <label for="subcategory">Subcategory</label>
                                    <select onload="" class="custom-select" id="subcategory" name="subcategory" required>
                                        <option hidden="true">${product.subcategoryDto.name}</option>
                                        <%--         <c:forEach var="s" items="${product.subcategoryDto.categoryDto.subcategoryDto}">
                                                     <option value="${s.id}"><c:out value="${s.name}"/></option>
                                                 </c:forEach> --%>
                                        <c:forEach var="c" items="${categories}">
                                            <c:if test="${product.subcategoryDto.categoryDto.id == c.id}">
                                                <c:forEach var="s" items="${c.subcategoryDto}">
                                                    <option value="${s.name}"><c:out value="${s.name}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </c:forEach>
                                    </select>
                                    <div class="invalid-feedback">Example invalid custom select feedback</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 mb-3">
                                    <label for="price">Price</label>
                                    <div class="input-group-prepend">
                                        <input type="number" class="form-control" id="price" name="price" value="${product.price}" min="1" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text">Ft</span>
                                        </div>
                                        <div class="invalid-feedback">
                                            Please provide a valid price!
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">

                                <label for="description">Description</label>
                                <textarea class="form-control" id="description" name="description" 
                                          required  rows="5">${product.description}</textarea>
                                <div class="invalid-feedback">Please provide a valid description!</div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 mb-3">
                                    <label for="image">Image</label>
                                    <input type="text" class="form-control" id="image" name="image" 
                                           value="${product.imagePath}" required>
                                    <div class="invalid-feedback">Please provide a valid URL!</div>
                                </div>   
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 mb-3">
                                    <label for="quantity">Quantity</label>
                                    <input type="number" class="form-control" id="quantity" name="quantity" 
                                           value="${product.quantity}" value="1" min="1" required>
                                    <div class="invalid-feedback">Please provide a valid product quantity!</div>
                                </div>
                            </div>
                            <input tpye="number" name="id" value="${product.id}" hidden="true">
                            <input type="text" name="action" value="update" hidden="true" id="action">
                        </a>

                        <div class="form-group">
                            <button class="btn btn-outline-primary" type="button" value="Modify" id="modifyButton"
                                    onclick="setDetailsHiddenAndModifyInputsVisible(this)">Modify</button>
                        </div>
                    </form>
                    <div class="form-group">
                        <form method="post" action="productDetails">
                            <!-- az adott product id-j�t rejtve elt�roljuk, �gy k�s?bb a servlet tudja
                                    majd melyik term�ket kell inakt�vv� tenni -->
                            <input tpye="number" name="id" value="${product.id}" hidden="true">
                            <!-- a formhoz tartoz� action rejtett, a servlet ez alapj�n
                                    d�nti majd el hogy t�rl�nk -->
                            <input type="text" name="action" value="delete" hidden="true" id="action">
                            <div class="form-group">
                                <button class="btn btn-outline-dark" type="button" onclick="askForConfirmation(this)" value="Delete" id="deleteButton">Delete</button>
                            </div>
                        </form>
                    </div>
                    <div>
                        <form method="post" action="cart">
                            <input tpye="number" name="id" value="${product.id}" hidden="true">
                            <input type="text" name="action" value="addToCart" hidden="true">
                            <Button class="btn btn-outline-dark" type="submit" value="Add to cart" id="addToCartButton">Add to cart</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="/menu/footer.jsp"%>
    </body>
</html>
