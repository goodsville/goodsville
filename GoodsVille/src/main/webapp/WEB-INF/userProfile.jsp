<%-- 
    Document   : userProfile
    Created on : 2020. márc. 7., 17:22:00
    Author     : andra
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>User profile</title>
        <%@include file="/menu/head.jsp"%>
        <script type="text/javascript">
            function setIntentionLogout() {
            <c:set var="intention" scope="request" value="logout"/>
            }
            function getUserData() {
                var userDetailRef = document.getElementById("userDetailResult");
                fetch("${pageContext.request.contextPath}/user").then((response) =>
                {
                    return response.json();
                }

                ).then(
                        (objectum) => {

                    for (var i = 0; i < Object.keys(objectum).length; i++) {
                        var obj = objectum;
                        var objKey = Object.keys(obj)[i];
                        var objValue = obj[objKey];
                        
                        if (objKey === "password") {
                            continue;
                        }
                        userDetailRef.insertAdjacentHTML('beforeend', "<strong>" + objKey + ": " + "</strong>");
                        userDetailRef.insertAdjacentHTML('beforeend', objValue + "</br>");
                    }
                }
                )
            }
            ;


            function doEditable() {
                var userDetailRef = document.getElementById("userDetailResult");
                userDetailRef.insertAdjacentHTML('afterbegin', "<form id=\"userInput\" method=\"post\" action=\"${pageContext.request.contextPath}/userProfile></form>");
                var userInputForm = document.getElementById("userInput");

                fetch('${pageContext.request.contextPath}/user').then((response) =>
                {
                    return response.json();
                }

                ).then(
                        (objectum) => {

                    for (var i = 0; i < Object.keys(objectum).length; i++) {
                        var obj = objectum;
                        var objKey = Object.keys(obj)[i];
                        var objValue = obj[objKey];


                        userInputForm.insertAdjacentHTML('beforeend', "<label for \"" + objKey + "> <strong>" + objKey + ": " + "</strong></label>");
                        userInputForm.insertAdjacentHTML('beforeend', "<input type=\"text\" name=\"" + objKey + "\" value=\"" + objValue + "\"" + "></br>");

                    }
                    userInputForm.insertAdjacentHTML('beforeend', "<input type=\"hidden\" name=\"overrideMethod\" value=\"put\" />");
                    userInputForm.insertAdjacentHTML('beforeend', "<input type=\"submit\" value=\"submit\">");
                }
                )

            }
            ;

        </script>
    </head>
    <body onload="getUserData()">
        <%@include file="../menu/menu.jsp"%>
        <c:set var="user" value= "${sessionScope.currentuserInSession}"/>

        <<!-- <a onclick="setIntentionLogout()" class="nav-link" href="/GoodsVille/login">Logout</a> //menu.jsp-be beépíteni a Login/Register helyett ha be van jelentkezve-->>

        <div class="container p-5">
            <div class="row">
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-header text-center" style="size: 100%">
                            <strong>Your Profile</strong>
                        </div>		
                        <div class="card-body text-center">
                            <h4 class="card-title"> <c:out value="${currentUserInSession.firstName}"/> <c:out value="${currentUserInSession.lastName}"/> </h4>
                            <img class="card-img-top" src="https://www.pngrepo.com/png/5125/170/avatar.png" alt="profile image" style="width:100%">	
                            <div class="card-text text-left" id="userDetailResult">
  
                            </div>

                        </div>
                        <div class="card-footer text-center" style="size: 100%">
                            <a class="btn btn-primary" id="editProfileLink" onclick="doEditable()"> Edit profile </a>
                        </div>     
                    </div>
                </div>


                <div class="col-lg-9 pt-5">
                    <div class="row" id="cards">
                        <c:forEach var= "p" items="${productlist}">
                            <div class="col-lg-4 col-md-6 mb-4">
                                <div class="card h-100">
                                    <a href="#"><img class="card-img-top" src="<c:out value="${p.imagePath}"/>" alt="" width="700" height="400"></a>
                                    <div class="card-body">
                                        <h4 class="card-title">
                                            <a href="#"><c:out value="${p.name}"/></a>
                                        </h4>
                                        <h5><c:out value="${p.price} Ft" /></h5>
                                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur!</p>
                                        <a href="${pageContext.request.contextPath}\productDetails?id=${p.id}" class="btn btn-link" role="button">Details</a>
                                    </div>
                                    <div class="card-footer">
                                        <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>

        <%@include file="/menu/footer.jsp"%>
    </body>
</html>
