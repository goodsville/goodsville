<!DOCTYPE html>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
    <head>
        <title>New Product</title>
        <%@include file="/menu/head.jsp"%>
        <script>
            (function () {
                'use strict';
                window.addEventListener('load', function () {
                    var forms = document.getElementsByClassName('needs-validation');
                    var validation = Array.prototype.filter.call(forms, function (form) {
                        form.addEventListener('submit', function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
        </script>
        <script>
            function getSelectedCategory() {
                var c = document.getElementById("category");
                var categoryId = c.options[c.selectedIndex].value.toString();
                var subcategorySelect = document.getElementById("subcategory");
                subcategorySelect.innerHTML = "";
                subcategorySelect.insertAdjacentHTML('beforeend', "<option value=\"\">" + "Open this select menu" + " </option>");
            <c:forEach var="c" items="${categories}">
                var externalCatId = "${c.id}";
                <c:forEach var="s" items="${c.subcategoryDto}">
                if (categoryId === externalCatId) {
                    subcategorySelect.insertAdjacentHTML('beforeend', "<option value=\"" + "${s.name}" + "\"" + ">" + "${s.name}" + "</option>");
                }
                </c:forEach>
            </c:forEach>
            }
            ;
        </script>
    </head>
    <body>
        <%@ include file="../menu/menu.jsp" %>
        <div class="containter pt-3"></div>
        <div class="container pt-5">
            <h3>Fill the form to add new product</h3>
            <div class="container">
                <form class="needs-validation" method="post" action="newProduct"  novalidate>
                    <div class="form-group">
                        <div class="col-md-4 mb-3">
                            <label for="productName">Product name</label>
                            <input type="text" class="form-control" id="productName" name="productName" placeholder="Product name" required>
                            <div class="invalid-feedback">Please provide a valid product name!</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4 mb-3">
                            <label for="category">Category</label>
                            <select onchange="getSelectedCategory()" class="custom-select" id="category" name="category" required>
                                <option value="">Open this select menu</option>
                                <c:forEach var="c" items="${categories}">
                                    <option value="${c.id}"><c:out value="${c.name}"/></option>
                                </c:forEach>
                            </select>
                            <div class="invalid-feedback">Example invalid custom select feedback</div>
                        </div>
                    </div>
                    <div id="selectedcategory"></div>
                    <div class="form-group">
                        <div class="col-md-4 mb-3">
                            <label for="subcategory">Subcategory</label>
                            <select class="custom-select" id="subcategory" name="subcategory" required>
                            </select>
                            <div class="invalid-feedback">Example invalid custom select feedback</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4 mb-3">
                            <label for="price">Price</label>
                            <div class="input-group-prepend">
                                <input type="number" class="form-control" id="price" name="price" placeholder="Price" min="1" required>
                                <div class="input-group-append">
                                    <span class="input-group-text">Ft</span>
                                </div>
                                <div class="invalid-feedback">
                                    Please provide a valid price!
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" name="description" placeholder="Description" required  rows="5"></textarea>
                        <div class="invalid-feedback">Please provide a valid description!</div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4 mb-3">
                            <label for="image">Image</label>
                            <input type="text" class="form-control" id="image" name="image" placeholder="Image URL" required>
                            <div class="invalid-feedback">Please provide a valid URL!</div>
                        </div>   
                    </div>
                    <div class="form-group">
                        <div class="col-md-4 mb-3">
                            <label for="quantity">Quantity</label>
                            <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Quantity" value="1" min="1" required>
                            <div class="invalid-feedback">Please provide a valid product quantity!</div>
                        </div>
                    </div>   
                    <div class="form-group">
                        <input class="btn btn-outline-dark" type="submit" value="Submit"/>
                    </div>
                </form>    
            </div>
        </div>
        <%@include file="/menu/footer.jsp"%>
    </body>
</html>
