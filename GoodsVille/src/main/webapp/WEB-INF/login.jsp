<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Login page</title>
        <%@include file="/menu/head.jsp"%>
    </head>
    <body>
 <%@ include file="../menu/menu.jsp" %>
       

<div class="containter p-5">
    <div class="modal-content loginpage">
        <form class="animate" action="j_security_check" method="post">
            <div class="imgcontainer">
                <img src="https://www.pngrepo.com/png/5125/170/avatar.png" alt="Avatar" class="avatar">
            </div>
            <div class="container login">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="j_username" id="username" required>
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="j_password" id="password" required>
                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn login-button" >
                            Login
                        </button>
                    </div>
                </div>
                <label>
                    <input type="checkbox" checked="checked" name="remember"> Remember me
                </label>
            </div>
            <div class="container login" >     
                <button type="submit" class="btn btn-outline-info">Registration</button>
                <span class="psw">Forgot <a href="#">password?</a></span>

            </div>
        </form>
    </div>
</div>

    </body>
    <%@include file="/menu/footer.jsp"%>
</html>
