<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <%@include file="/menu/head.jsp"%>
    </head>
    <body>
    <%@ include file="../menu/menu.jsp" %>
        <div class="container p-3">
            <div class="container pt-5">
                <h2>Frequently asked questions</h2>
                <button type="button" class="btn btn-light custom" data-toggle="collapse" data-target="#demo1">How do I make a purchase?</button>
                <div id="demo1" class="collapse">
                    Making a purchase at GoodsVille is quick, easy and secure.
                    When you find an item you want to purchase, click the ‘Add to Cart’ button. The item will be added to “Your Cart” this is found on the top right of the page.
                    When you are ready to finalise your order, click the ‘Checkout’ button in the Your Cart Section
                    Enter Shipping & Payment information, to complete your purchase.
                </div>
                <button type="button" class="btn btn-light custom" data-toggle="collapse" data-target="#demo2">What are the available Payment Modes?</button>
                <div id="demo2" class="collapse">
                    You can contact the seller for Payment information
                </div>
                <button type="button" class="btn btn-light custom" data-toggle="collapse" data-target="#demo3">How long will delivery take?</button>
                <div id="demo3" class="collapse">
                    Delivery time will vary with your location. Check from our representative on call by telling us your pincode.
                </div> 
                <button type="button" class="btn btn-light custom" data-toggle="collapse" data-target="#demo4">How secure is shopping in the Online Shop? Is my data protected?</button>
                <div id="demo4" class="collapse">
                    We respect your privacy we do not share your details.
                </div> 
            </div>
        </div>
        <%@include file="/menu/footer.jsp"%>
    </body>
</html>
