<%-- 
    Document   : cart
    Created on : 2020.03.22., 21:38:35
    Author     : gabri
--%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/menu/head.jsp"%>
        <title>Cart Page</title>

        <script>
            function calculateCartValue() {

                var totalItemValue = 0;
                const itemValues = document.querySelectorAll(".itemValue");
                for (const itemValue of itemValues) {

                    totalItemValue += parseInt(itemValue.textContent);
                }
                ;
                var totalPayablePlace = document.getElementById("totalPayable");
                totalPayablePlace.innerHTML = "<b>" + totalItemValue + "</b>" + " Ft";


            }


            function recalculatePriceAndCartValue(cartItemID) {

                var cartItemID = cartItemID;
                var quantityWasPutNew = document.getElementById(cartItemID + "itemQuantity").value;
                /*var valueOfItemOld = parseInt(document.getElementById(cartItemID + "itemValue").innerText);
                 var totalPayableOld = parseInt(document.getElementById("totalPayableOrigin").innerText);*/
                var priceOfItemPlace = document.getElementById(cartItemID + "itemPrice");
                var priceOfItem = parseInt(priceOfItemPlace.innerText);

                var valueOfItemPlace = document.getElementById(cartItemID + "itemValue");

                var valueOfItemNew = quantityWasPutNew * priceOfItem;

                /*var change = valueOfItemNew - valueOfItemOld;
                 var totalPayableNew = totalPayableOld + change;*/


                valueOfItemPlace.innerHTML = valueOfItemNew + " Ft";
                calculateCartValue();

            }

            function calculateItemValues() {
                const itemValues = document.querySelectorAll(".itemValue");

                for (const itemValue of itemValues) {
                    var cartItemID = (itemValue.id).replace("itemValue", "");
                    recalculatePriceAndCartValue(cartItemID);
                }
            }

        </script>
    </head>
    <body onload="calculateItemValues();">
   <%@ include file="../menu/menu.jsp" %>
        <div class="container p-3">
            <div class="container pt-5">
                <form method="post" action="purchase">
                    <c:forEach var= "cartItem" items="${cartItems}">
                        <div class="container">
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-12 col-sm-12 col-md-2 text-center">
                                        <img class="img-responsive" src="<c:out value="${cartItem.imagePath}"/>" alt="prewiew" width="80" height="120">
                                    </div>

                                    <div class="col-12 text-sm-center col-sm-12 text-md-left col-md-6" id=${cartItem.id}itemId>
                                        <h3><strong><c:out value="${cartItem.name}"/></strong></h3>

                                        <p><small>Seller name: <c:out value="${cartItem.userDto.userName}"/></small></p>
                                        <p><small>City: <c:out value="${cartItem.userDto.city}"/></small></p>
                                    </div>

                                    <div class="col-12 col-sm-12 text-sm-center col-md-4 text-md-right row">
                                        <div class="col-3 col-sm-3 col-md-6 text-md-right" style="padding-top: 5px">
                                            <div class="cartValueMember">
                                                <h6>
                                                    <strong> 
                                                        <span id=${cartItem.id}itemPrice>${cartItem.price}</span> 
                                                        <span>Ft</span><span class="text-muted"> x</span>
                                                    </strong>
                                                </h6>
                                            </div>
                                        </div>
                                        <div class="col-4 col-sm-4 col-md-4">
                                            <div class="quantity">
                                                <div class="cartValueMember">
                                                    <input id=${cartItem.id}itemQuantity onchange="recalculatePriceAndCartValue(${cartItem.id})" type="number" step="1" max="${cartItem.quantity}" min="1" value="1" class="qty"
                                                           size="4" name="${cartItem.name}"/>
                                                    <div>sum:</div>
                                                    <div class="cartValueMember">
                                                        <div class="itemValue" id=${cartItem.id}itemValue> </div>
                                                    </div>
                                                </div>
                                            </div>    
                                        </div>

                                        <div class="col-2 col-sm-2 col-md-2 text-right">
                                            <input tpye="number" name="id" value="${cartItem.id}" hidden="true">
                                            <button onclick="calculateCartValue()" type="submit" name="action" value="deleteFromCart" class="btn btn-outline-danger btn-xs">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </c:forEach>          
                    <div class="card-footer">
                        <div class="pull-right">
                            <h6 class="pull-right"> Total price: <b><p id="totalPayable"></p></b></h6>
                            <br>
                            <button type="submit" name="action" value="finishPurchase" class="btn btn-outline-secondary pull-right">Finish purchase</button>

                        </div>
                </form>
                <div class="container p-5">
                </div>


            </div>
        </div>

        <div class="container p-3">

        </div>
    </div>
    <%@ include file="/menu/footer.jsp" %>
</body>
</html>
