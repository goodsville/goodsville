<%-- 
    Document   : purchase
    Created on : 2020.03.28., 0:54:43
    Author     : gabri
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/menu/head.jsp"%>
        <title>Purchase Page</title>


    </head>
    <body>
       <%@ include file="../menu/menu.jsp" %>

        <div class="container p-5">

            <div class="container p-3">
                <div class="row">
                    <div class="col-lg-9 pt-5">

                        <h3>Your purchase was successfull!</h3>
                        <h6>Please contact the seller for more information!</h6>
                        <div class="container pt-3">
                        <table class="table table-hover">

                            <thead>
                                <tr>
                                    <th>Item: </th>
                                    <th>Price: </th>
                                    <th>Seller username: </th>
                                    <th>Seller name: </th>
                                    <th>Seller email: </th>
                                    <th>Seller phone: </th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="history" items="${history}">
                                    <tr>
                                        <td><c:out value="${history.name}"/></td>
                                        <td><c:out value="${history.price}"/></td>
                                        <td><c:out value="${history.userDto.userName}"/></td>
                                        <td><c:out value="${history.userDto.firstName}"/> <c:out value="${history.userDto.lastName}"/> </td>
                                        <td><c:out value="${history.userDto.email}"/></td>
                                        <td><c:out value="${history.userDto.mobile}"/></td>
                                    </tr>
                                </tbody>
                            </c:forEach>
                        </table>
                    </div>

                    <div class="col-lg-3 col-lg-offset align-items-center">
                        <div class="h-100 d-flex justify-content-center align-items-center">
                            <img src='<c:url value="/img/payinglogo.svg" />' class="img-fluid"  alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <%@include file="/menu/footer.jsp"%>
</body>
</html>
