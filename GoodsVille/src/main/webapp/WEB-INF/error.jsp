<%-- 
    Document   : errorG
    Created on : 2020.03.30., 9:39:23
    Author     : gabri
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
        <%@include file="/menu/head.jsp"%>
        <link rel="stylesheet" type="text/css" href="style/404.css">
        <title>Error Page</title>
    </head>
    <body>

        <div class="page-wrap d-flex flex-row align-items-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12 text-center">
                        <span class="display-1 d-block"><img src='<c:url value="/img/404logoWithoutbcg.svg" />' class="img-fluid"  alt=""/></span>
                        <div class="mb-4 lead">The page you are looking for was not found.</div>
                        <a href=${pageContext.request.contextPath}\home class="btn btn-link">Back to Home</a>
                    </div>
                </div>
            </div>
        </div>

</html>
