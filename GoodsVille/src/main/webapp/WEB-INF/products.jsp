<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page import="hu.braininghub.bh11.goodsville.dto.ProductDto"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <script>
            function messageFromProductCreating() {
                if ("${msg}" !== "") {
                    if ("${msg}" === "siker") {
                        alert("Siker! Terméked mostantól látható a kínálatban");
                    } else {
                        alert("Hiba történt a terméked létrehozásakor");
                    }
                }
            }
            function getSelectedCategory() {
                var c = document.getElementById("category");
                var categoryId = c.options[c.selectedIndex].value.toString();
                var subcategorySelect = document.getElementById("subcategory");
                if (c.value === "none") {
                    subcategorySelect.innerHTML = "";
                    subcategorySelect.insertAdjacentHTML('beforeend', "<option value=\"\">Open this select menu</option>");
            <c:forEach var="c" items="${categories}">
                <c:forEach var="s" items="${c.subcategoryDto}">
                    subcategorySelect.insertAdjacentHTML('beforeend', "<option value=\"" + "${s.name}" + "\"" + ">" + "${s.name}" + "</option>");
                </c:forEach>
            </c:forEach>
                } else {
                    subcategorySelect.innerHTML = "";
                    subcategorySelect.insertAdjacentHTML('beforeend', "<option value=\"\">" + "Open this select menu" + " </option>");
            <c:forEach var="c" items="${categories}">
                    var externalCatId = "${c.id}";
                <c:forEach var="s" items="${c.subcategoryDto}">
                    if (categoryId === externalCatId) {
                        subcategorySelect.insertAdjacentHTML('beforeend', "<option value=\"" + "${s.name}" + "\"" + ">" + "${s.name}" + "</option>");
                    }
                </c:forEach>
            </c:forEach>
                }
            }
        </script>

        <title>Products</title>
        <%@include file="/menu/head.jsp"%>
    </head>
    <body onload="messageFromProductCreating()">
 <%@ include file="../menu/menu.jsp" %>
        <div class="container pt-5">
            <div class="row">
                <div class="col-lg-3 pt-5">
                    <h1 class="my-4">GoodsVille</h1>
                    <div class="list-left-column">
                        <div class="clearfix">
                            <form class="ng-pristine ng-valid searchbox_form" id="filter_form"
                                  method="post" name="filter_form">
                                <div class="sbContainer noextended">
                                    <img src='<c:url value="/img/searchlogoWithoutbcg.svg" />'/>
                                    <div class="nativetext input-wrapper">

                                        <label class="label" for="productName">Product Name</label>
                                        <div class="form-group">
                                            <div class="col-md-12 mb-9">
                                                <input class="text" id="pname" name="pname" type="text" placeholder="Keyword" autocomplete="off">
                                            </div>
                                        </div>

                                        <label for="category">Category</label>
                                        <div class="form-group">
                                            <div class="col-md-12 mb-9">
                                                <select onchange="getSelectedCategory()" class="custom-select" id="category" name="category">
                                                    <option value="none">Open this select menu</option>
                                                    <c:forEach var="c" items="${categories}">
                                                        <option value="${c.id}"><c:out value="${c.name}"/></option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>

                                        <label for="subcategory">Subcategory</label>
                                        <div class="form-group">
                                            <div class="col-md-12 mb-3">
                                                <select class="custom-select" id="subcategory" name="subcategory">
                                                    <option value="">Open this select menu</option>
                                                    <c:forEach var="c" items="${categories}">
                                                        <c:forEach var="s" items="${c.subcategoryDto}">
                                                            <option value="${s.name}"> <c:out value="${s.name}"/></option>
                                                        </c:forEach>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>

                                        <label class="label" for="price" title="Price">Price</label>
                                        <div class="form-group">
                                            <div class="col-md-12 mb-3">
                                                <input class="text" id="price" name="priceMin" type="number" placeholder="Min">
                                                <input class="text" id="price" name="priceMax" type="number" placeholder="Max">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 mb-3">
                                                <div class="checkbox">
                                                    <input type="checkbox" name="justWithImage" />
                                                    <label class="checkbox"></label>
                                                    <label class="checkbox-label">Just with photo</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 mb-3">
                                                <div class="checkbox">
                                                    <input type="checkbox" name="description" />
                                                    <label class="checkbox-checker"></label>
                                                    <label class="checkbox-label">Searching in description too</label>
                                                </div>
                                            </div>
                                        </div>

                                        <label class="label" for="quality" title="Quality">Quality</label>
                                        <div class="form-group">
                                            <div class="col-md-12 mb-9">
                                                <input class="text" id="quality" name="quality" type="number" placeholder="Quality" min="1" max="5">
                                            </div>
                                        </div>
                                        <label class="label" for="city" title="City">City</label>
                                        <div class="form-group">
                                            <div class="col-md-12 mb-3">
                                                <input class="text" id="city" name="city" type="text" placeholder="Global" autocomplete="off">
                                            </div>
                                        </div>
                                        <label class="label" for="userName" title="Seller Name">Seller Name</label>
                                        <div class="form-group">
                                            <div class="col-md-12 mb-3">
                                                <input class="text" id="userName" name="userName" type="text" placeholder="All sellers" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="foot">
                                                <input type="submit" value="Filter" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


                <div class="col-lg-9 pt-5">
                    <div class="row" id="cards">
                        <c:forEach var= "p" items="${productlist}">
                            <div class="col-lg-4 col-md-6 mb-4">
                                <div class="card h-100">
                                    <a href="#"><img class="card-img-top" src="<c:out value="${p.imagePath}"/>" alt="" width="700" height="400"></a>
                                    <div class="card-body">
                                        <h4 class="card-title">
                                            <a href="#"><c:out value="${p.name}"/></a>
                                        </h4>
                                        <h5><c:out value="${p.price} Ft" /></h5>
                                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur!</p>
                                        <a href="${pageContext.request.contextPath}\productDetails?id=${p.id}" class="btn btn-link" role="button">Details</a>
                                    </div>
                                    <div class="card-footer">
                                        <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>


        <%@include file="/menu/footer.jsp"%>
    </body>
</html>