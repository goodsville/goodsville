<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@include file="loginModal.jsp" %>

<nav class="navbar navbar-expand-sm navbar-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href=${pageContext.request.contextPath}\home>GoodsVille</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link"  href=${pageContext.request.contextPath}\home>Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href=${pageContext.request.contextPath}\products>Products</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="faq" href=${pageContext.request.contextPath}\FAQ>FAQ</a>
                </li>
                <li class="nav-item">
                    <div class="dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">${pageContext.request.remoteUser}</a>
                        <div class="dropdown-menu">

                            <form action="login" method="POST">
                                <input  class="dropdown-item" type="submit" id="logoutLink" value="Logout">            
                            </form>

                            <a class="dropdown-item" id="userProfileLink" href="${pageContext.request.contextPath}\userProfile"> ${pageContext.request.remoteUser}'s Profile </a>
                            <a class="dropdown-item" id="userProfileLink" href="${pageContext.request.contextPath}\newProduct"> Add products </a>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <c:set var="isAnyProductInCart" value="${cartItems.size() > 0}"/>
                    <c:choose>
                        <c:when test="${isAnyProductInCart}">
                            <a class="nav-link" id="cartIcon" href=${pageContext.request.contextPath}\cart><i class="fas fa-shopping-cart"> (${cartItems.size()})</i></a>
                            </c:when>
                            <c:otherwise>
                            <a class="nav-link" id="cartIcon" href=${pageContext.request.contextPath}\cart><i class="fas fa-shopping-cart"></i></a>
                            </c:otherwise>
                        </c:choose>  
                </li>
            </ul>
        </div>
    </div> 

</nav>
