<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

        <c:set var="isAnyUser" value="${(pageContext.request.remoteUser != null)}"/>
        <c:choose>
            <c:when test="${isAnyUser}">
                <%@ include file="menuAfterAuthentication.jsp" %>
            </c:when>
            <c:otherwise>
                <%@ include file="menuBeforeAuthentication.jsp" %>
            </c:otherwise>
        </c:choose>