
<div id="userLoginModal" class="modal">
    <div class="modal-content">
        <form class="animate" action="j_security_check" method="post">
            <div class="imgcontainer">
                <span onclick="document.getElementById('userLoginModal').style.display = 'none'" class="close" title="Close Modal">&times;</span>
                <img src="https://www.pngrepo.com/png/5125/170/avatar.png" alt="Avatar" class="avatar">
            </div>
            <div class="container login">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="j_username" id="username" required>
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="j_password" id="password" required>
                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn login-button" >
                            Login
                        </button>
                    </div>
                </div>
                <label>
                    <input type="checkbox" checked="checked" name="remember"> Remember me
                </label>
            </div>
            <div class="container login" >     
                <button type="submit" onclick="document.getElementById('userLoginModal').style.display = 'none'" class="btn btn-outline-danger cancelbtn">Cancel</button>
                <span class="psw">Forgot <a href="#">password?</a></span>

            </div>
        </form>
    </div>
</div>
