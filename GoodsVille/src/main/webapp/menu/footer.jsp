
<footer>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-4 footer-about pt-3">
                    <img class="logo-footer" src='<c:url value="/img/mobilelogoWithoutbcg.svg"/>' alt="logo-footer"/>
                </div>
                <div class="col-md-4 col-lg-4 offset-lg-1 footer-contact pt-5">
                    <h3>Contact</h3>
                    <p><i class="fas fa-map-marker-alt"></i> 1134, Budapest Klapka u. 11.</p>
                    <p><i class="fas fa-phone"></i> Phone: +36 70 703 1933</p>
                    <p><i class="fas fa-envelope"></i> Email: <a href="mailto:bh11@goodsville.com">bh11@goodsville.com</a></p>
                    <p><i class="fab fa-skype"></i> Skype: you_online</p>
                </div>
                <div class="col-md-4 col-lg-3 footer-contact pt-5">
                    <h3>Follow us</h3>
                    <a href="#"><i class="fab fa-facebook"></i></a> 
                    <a href="#"><i class="fab fa-twitter"></i></a> 
                    <a href="#"><i class="fab fa-google-plus-g"></i></a> 
                    <a href="#"><i class="fab fa-instagram"></i></a> 
                    <a href="#"><i class="fab fa-pinterest"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>
