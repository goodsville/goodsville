/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.goodsville.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.Mockito.mock;
import org.mockito.junit.jupiter.MockitoExtension;


/**
 *
 * @author BlOOOD
 */

@ExtendWith(MockitoExtension.class)
public class JPQLPatternBuilderTest {
    
    private JPQLPatternBuilder underTest;
    
    @Test
    public void testAddIntAttributeToQuery(){
        //Given
        String name = null;
        Integer categoryId = 0;
        String subcategoryName = null;
        String justWithImage = null;
        String searchInDescriptionToo = null;
        Integer priceMin = 0;
        Integer priceMax = 0; 
        Integer quality = 4;
        String city = null;
        String userName = null;
                
        underTest = new JPQLPatternBuilder(name, categoryId, 
                subcategoryName, justWithImage, searchInDescriptionToo, 
                priceMin, priceMax, quality, city, userName);
        String result = underTest.toString();
        
        //When
        
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT p FROM Product p WHERE p.active = 1 AND p.quality = ");
        sb.append(quality);
        
        //Then
        assertEquals(sb.toString(),result);
    }
    
    @Test
    public void testAddStringAttributeToQuery(){
        //Given
        String name = null;
        Integer categoryId = 0;
        String subcategoryName = "x";
        String justWithImage = null;
        String searchInDescriptionToo = null;
        Integer priceMin = 0;
        Integer priceMax = 0; 
        Integer quality = 0;
        String city = null;
        String userName = null;
                
        underTest = new JPQLPatternBuilder(name, categoryId, 
                subcategoryName, justWithImage, searchInDescriptionToo, 
                priceMin, priceMax, quality, city, userName);
        String result = underTest.toString();
        
        //When
        
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT p FROM Product p WHERE p.active = 1 AND p.subcategory.name LIKE '%");
        sb.append(subcategoryName);
        sb.append("%'");
        
        //Then
        assertEquals(sb.toString(),result);
    }
    
    @Test
    public void testAddSearchInDescriptionTooAttributeToQuery(){
        //Given
        String name = "x";
        Integer categoryId = 0;
        String subcategoryName = null;
        String justWithImage = null;
        String searchInDescriptionToo = "on";
        Integer priceMin = 0;
        Integer priceMax = 0; 
        Integer quality = 0;
        String city = null;
        String userName = null;
                
        underTest = new JPQLPatternBuilder(name, categoryId, 
                subcategoryName, justWithImage, searchInDescriptionToo, 
                priceMin, priceMax, quality, city, userName);
        String result = underTest.toString();
        
        //When
        
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT p FROM Product p WHERE p.active = 1 AND (p.name LIKE '%");
        sb.append(name);
        sb.append("%' OR p.description LIKE '%");
        sb.append(name);
        sb.append("%')");
        
        //Then
        assertEquals(sb.toString(),result);
    }
    
    @Test
    public void testAddPriceMinAttributeToQuery(){
        //Given
        String name = null;
        Integer categoryId = 0;
        String subcategoryName = null;
        String justWithImage = null;
        String searchInDescriptionToo = null;
        Integer priceMin = 1;
        Integer priceMax = 0; 
        Integer quality = 0;
        String city = null;
        String userName = null;
                
        underTest = new JPQLPatternBuilder(name, categoryId, 
                subcategoryName, justWithImage, searchInDescriptionToo, 
                priceMin, priceMax, quality, city, userName);
        String result = underTest.toString();
        
        //When
        
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT p FROM Product p WHERE p.active = 1 AND p.price > ");
        sb.append(priceMin);
        
        //Then
        assertEquals(sb.toString(),result);
    }
    
    @Test
    public void testAddPriceMaxAttributeToQuery(){
        //Given
        String name = null;
        Integer categoryId = 0;
        String subcategoryName = null;
        String justWithImage = null;
        String searchInDescriptionToo = null;
        Integer priceMin = 0;
        Integer priceMax = 1; 
        Integer quality = 0;
        String city = null;
        String userName = null;
                
        underTest = new JPQLPatternBuilder(name, categoryId, 
                subcategoryName, justWithImage, searchInDescriptionToo, 
                priceMin, priceMax, quality, city, userName);
        String result = underTest.toString();
        
        //When
        
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT p FROM Product p WHERE p.active = 1 AND p.price < ");
        sb.append(priceMax);
        
        //Then
        assertEquals(sb.toString(),result);
    }
    
    @Test
    public void testJustWithImageAttributeToQuery(){
        //Given
        String name = null;
        Integer categoryId = 0;
        String subcategoryName = null;
        String justWithImage = "on";
        String searchInDescriptionToo = null;
        Integer priceMin = 0;
        Integer priceMax = 0; 
        Integer quality = 0;
        String city = null;
        String userName = null;
                
        underTest = new JPQLPatternBuilder(name, categoryId, 
                subcategoryName, justWithImage, searchInDescriptionToo, 
                priceMin, priceMax, quality, city, userName);
        String result = underTest.toString();
        
        //When
        String means = "SELECT p FROM Product p WHERE p.active = 1 AND p.imagePath IS NOT NULL";
        
        //Then
        assertEquals(means,result);
    }
    
}
