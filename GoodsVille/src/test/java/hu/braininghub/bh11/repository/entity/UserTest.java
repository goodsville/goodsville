/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.repository.entity;

import hu.braininghub.bh11.goodsville.dto.UserDto;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.beanutils.BeanUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author andra
 */
@ExtendWith(MockitoExtension.class)
public class UserTest {

    private User underTest;

    @BeforeEach
    void init() {
        underTest = new User();

    }
    @Disabled
    @Test
    public void testUserSetDataWitAllData() throws IllegalAccessException, InvocationTargetException {
        //Given
        User userM = mock(User.class);
        UserDto userDtoM = new UserDto();

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<String> userNameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> passwordCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> emailCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Boolean> isAdminCaptor = ArgumentCaptor.forClass(Boolean.class);
        ArgumentCaptor<String> firstNameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> lastNameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> mobileCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<String> cityCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> addressCaptor = ArgumentCaptor.forClass(String.class);

        //When
        doNothing().when(userM).setId(idCaptor.capture());
        doNothing().when(userM).setUserName(userNameCaptor.capture());
        doNothing().when(userM).setPassword(passwordCaptor.capture());
        doNothing().when(userM).setEmail(emailCaptor.capture());
        doNothing().when(userM).setIsAdmin(isAdminCaptor.capture());
        doNothing().when(userM).setFirstName(firstNameCaptor.capture());
        doNothing().when(userM).setLastName(lastNameCaptor.capture());
        doNothing().when(userM).setMobile(mobileCaptor.capture());
        doNothing().when(userM).setCity(cityCaptor.capture());
        doNothing().when(userM).setAddress(addressCaptor.capture());

        userM.setId(1);
        userM.setUserName("userName");
        userM.setPassword("password");
        userM.setEmail("email");
        userM.setIsAdmin(Boolean.FALSE);
        userM.setFirstName("firstName");
        userM.setLastName("lastName");
        userM.setMobile(12345);
        userM.setCity("city");
        userM.setAddress("address");

        BeanUtils.copyProperties(userDtoM, userM);
        
        underTest.setId(1);
        underTest.setUserName("userName");
        underTest.setPassword("password");
        underTest.setEmail("email");
        underTest.setIsAdmin(Boolean.FALSE);
        underTest.setFirstName("firstName");
        underTest.setLastName("lastName");
        underTest.setMobile(12345);
        underTest.setCity("city");
        underTest.setAddress("address");

        //Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(1, idCaptor.getValue()),
                () -> Assertions.assertEquals("userName", userNameCaptor.getValue()),
                () -> Assertions.assertEquals("password", passwordCaptor.getValue()),
                () -> Assertions.assertEquals("email", emailCaptor.getValue()),
                () -> Assertions.assertEquals(Boolean.FALSE, isAdminCaptor.getValue()),
                () -> Assertions.assertEquals("firstName", firstNameCaptor.getValue()),
                () -> Assertions.assertEquals("lastName", lastNameCaptor.getValue()),
                () -> Assertions.assertEquals(12345, mobileCaptor.getValue()),
                () -> Assertions.assertEquals("city", cityCaptor.getValue()),
                () -> Assertions.assertEquals("address", addressCaptor.getValue()),
                () -> Assertions.assertEquals(underTest, userDtoM));

    }
}
