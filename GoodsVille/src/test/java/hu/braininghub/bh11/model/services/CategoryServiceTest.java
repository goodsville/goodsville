/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.model.services;

import hu.braininghub.bh11.goodsville.dto.CategoryDto;
import hu.braininghub.bh11.goodsville.dto.SubcategoryDto;
import hu.braininghub.bh11.goodsville.repository.CategoryDao;
import hu.braininghub.bh11.repository.entity.Category;
import hu.braininghub.bh11.repository.entity.Subcategory;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.beanutils.BeanUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author gabri
 */
@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {

    @Mock
    private CategoryDao dao;

    private CategoryService underTest;

    @BeforeEach
    void init() {
        underTest = new CategoryService();
        underTest.setDao(dao);
    }

    @Test
    public void testGetCategoriesWithOneItem() throws IllegalAccessException, InvocationTargetException {
        //Given
        List<CategoryDto> result = new ArrayList<>();
        Category spyCategory = Mockito.spy(Category.class);
        CategoryDto categoryDto = new CategoryDto();
        List<Subcategory> subcategories = new ArrayList<>();
        Subcategory subcategory = mock(Subcategory.class);
        subcategories.add(subcategory);
        spyCategory.setSubcategory(subcategories);
        BeanUtils.copyProperties(categoryDto, spyCategory);
        SubcategoryDto subcategoryDto = mock(SubcategoryDto.class);
        BeanUtils.copyProperties(subcategoryDto, subcategory);
        List<SubcategoryDto> subcategoryDtos = new ArrayList<>();
        subcategoryDtos.add(subcategoryDto);
        categoryDto.setSubcategoryDto(subcategoryDtos);
        result.add(categoryDto);
        //When
        when(dao.findAll()).thenReturn(Arrays.asList(spyCategory));
        List<CategoryDto> categories = underTest.getAllCategory();

        //Then
        Assertions.assertEquals(result, categories);
    }

    @Test
    public void testGetCategoriesWithEmptyList() {
        //Given
        when(dao.findAll()).thenReturn(new ArrayList<>());
        //When
        List<CategoryDto> products = underTest.getAllCategory();
        //Then
        Assertions.assertEquals(new ArrayList<>(), products);
    }

}
