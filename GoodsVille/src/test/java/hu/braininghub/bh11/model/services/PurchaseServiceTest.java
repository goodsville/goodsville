package hu.braininghub.bh11.model.services;

import hu.braininghub.bh11.goodsville.dto.ProductDto;
import hu.braininghub.bh11.goodsville.dto.UserDto;
import hu.braininghub.bh11.goodsville.repository.PurchaseDao;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

//  @author Istvan Palovics
@ExtendWith(MockitoExtension.class)
public class PurchaseServiceTest {

    @Mock
    private PurchaseDao dao;

    private PurchaseService underTest;

    @BeforeEach
    void init() {
        underTest = new PurchaseService();
        underTest.setPurchaseDao(dao);
    }
    
    @Test
    public void testCreateNewPurchaseWithOneItem() {
        //Given
        List<ProductDto> cart = new ArrayList<>();
        ProductDto spyProductDto = Mockito.spy(ProductDto.class);
        spyProductDto.setActive(1);
        spyProductDto.setPrice(0);
        spyProductDto.setQuantity(1);
        spyProductDto.setId(0);

        cart.add(spyProductDto);

        UserDto spyUserDto = Mockito.spy(UserDto.class);
        spyUserDto.setId(0);
        //When
        underTest.createNewPurchase(spyUserDto, cart);
        //Then     
    }

}
