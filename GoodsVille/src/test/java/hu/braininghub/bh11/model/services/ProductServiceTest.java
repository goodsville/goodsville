package hu.braininghub.bh11.model.services;

import hu.braininghub.bh11.goodsville.dto.CategoryDto;
import hu.braininghub.bh11.goodsville.dto.ProductDto;
import hu.braininghub.bh11.goodsville.dto.SubcategoryDto;
import hu.braininghub.bh11.goodsville.dto.UserDto;
import hu.braininghub.bh11.goodsville.dto.exceptions.NoSuchProductException;
import hu.braininghub.bh11.goodsville.repository.ProductDao;
import hu.braininghub.bh11.repository.entity.Category;
import hu.braininghub.bh11.repository.entity.Product;
import hu.braininghub.bh11.repository.entity.Subcategory;
import hu.braininghub.bh11.repository.entity.User;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.apache.commons.beanutils.BeanUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

    @Mock
    private ProductDao dao;

    private ProductService underTest;

    @BeforeEach
    void init() {
        underTest = new ProductService();
        underTest.setDao(dao);
    }

    @Test
    public void testGetProductsWithOneItem() throws IllegalAccessException, InvocationTargetException {
        //Given
        List<ProductDto> result = new ArrayList<>();
        User u = mock(User.class);
        Product spyProduct = Mockito.spy(Product.class);
        spyProduct.setUser(u);
        ProductDto dto = new ProductDto();
        BeanUtils.copyProperties(dto, spyProduct);
        UserDto uDto = mock(UserDto.class);
        dto.setUserDto(uDto);
        result.add(dto);
        when(dao.findAll()).thenReturn(Arrays.asList(spyProduct));
        //When
        List<ProductDto> products = underTest.getProducts();

        //Then
        Assertions.assertEquals(result, products);
    }

    @Test
    public void testGetProductWithEmptyList() {
        //Given
        when(dao.findAll()).thenReturn(new ArrayList<>());
        //When
        List<ProductDto> products = underTest.getProducts();
        //Then
        Assertions.assertEquals(new ArrayList<>(), products);
    }

    @Test
    public void testGetProductsByNameWithEmptyList() {
        //Given
        String name = "";
        when(dao.findByName(name)).thenReturn(new ArrayList<>());
        //When
        List<ProductDto> products = underTest.getProductsByName(name);
        //Then
        Assertions.assertEquals(new ArrayList<>(), products);
    }

    @Test
    public void testGetProductsByNameWithOneItem() throws IllegalAccessException, InvocationTargetException {
        //Given
        String name = "";
        List<ProductDto> result = new ArrayList<>();
        Product spyProduct = Mockito.spy(Product.class);
        User u = mock(User.class);
        Subcategory spySubcat = Mockito.spy(Subcategory.class);
        Category c = mock(Category.class);
        spySubcat.setCategory(c);
        spyProduct.setSubcategory(spySubcat);
        spyProduct.setUser(u);
        ProductDto dto = new ProductDto();
        when(dao.findByName(name)).thenReturn(Arrays.asList(spyProduct));
        BeanUtils.copyProperties(dto, spyProduct);
        UserDto uDto = mock(UserDto.class);
        dto.setUserDto(uDto);
        result.add(dto);
        //When
        List<ProductDto> products = underTest.getProductsByName(name);

        //Then
        Assertions.assertEquals(result, products);
    }

    @Test
    public void testGetProductByIdWithEmptyList() {
        //Given
        int id = 1;
        when(dao.findById(id)).thenReturn(Optional.empty());
        //When
        //Then
        Assertions.assertThrows(NoSuchProductException.class, () -> underTest.getProductById(id));
    }

    @Test
    public void testUpdateProductException() {
        //Given
        int id = 1;
        when(dao.findById(id)).thenReturn(Optional.empty());
        //When
        //Then
        Assertions.assertThrows(NoSuchProductException.class, () -> underTest.updateProduct("", "", 1, "", "", 1, 1));
    }

    @Test
    public void testDeleteProductException() {
        //Given
        int id = 1;
        when(dao.findById(id)).thenReturn(Optional.empty());
        //When
        //Then
        Assertions.assertThrows(NoSuchProductException.class, () -> underTest.deleteProduct(1));
    }

    @Test
    public void testGetProductByIdWithOneItem() throws IllegalAccessException, InvocationTargetException {
        //Given
        int id = 1;
        Product p = mock(Product.class);
        ProductDto dto = new ProductDto();
        User u = mock(User.class);
        Product spyProduct = Mockito.spy(Product.class);
        spyProduct.setUser(u);
        Subcategory spySubcat = Mockito.spy(Subcategory.class);
        Category c = mock(Category.class);
        spySubcat.setCategory(c);
        spyProduct.setSubcategory(spySubcat);
        when(dao.findById(id)).thenReturn(Optional.of(spyProduct));
        BeanUtils.copyProperties(dto, spyProduct);
        //When
        ProductDto product = underTest.getProductById(id);

        //Then
        Assertions.assertEquals(dto, product);
    }

    @Test
    public void testDeleteProduct() {
        //Given
        Product spyProduct = Mockito.spy(Product.class);
        spyProduct.setActive(1);
        spyProduct.setId(1);
        Integer expectedResult = 0;
        //When
//        when(dao.findById(1)).thenReturn(spyProduct);
        Mockito.doReturn(Optional.of(spyProduct)).when(dao).findById(1);
        underTest.deleteProduct(1);
        //Then
        Assertions.assertEquals(expectedResult, spyProduct.getActive());
    }

    @Test
    public void testUpdateProduct() {
        //Given
        Product spyProduct = Mockito.spy(Product.class);

        int testProductId = 1;
        int originalPrice = 1000;
        int updatedPrice = 2000;
        int originalQuantity = 1;
        int updatedQuantity = 2;

        spyProduct.setId(testProductId);
        spyProduct.setName("TestName");
        spyProduct.setImagePath("TestPath");
        spyProduct.setPrice(originalPrice);
        spyProduct.setQuantity(originalQuantity);
        spyProduct.setDescription("TestDescription");
        //When
        Mockito.doReturn(Optional.of(spyProduct)).when(dao).findById(testProductId);
        underTest.updateProduct("UpdatedName", "", updatedPrice, "UpdatedDescription", "UpdatedPath", updatedQuantity, testProductId);
        //Then
        Assertions.assertTrue("UpdatedName".equals(spyProduct.getName())
                && spyProduct.getPrice() == updatedPrice
                && "UpdatedDescription".equals(spyProduct.getDescription())
                && "UpdatedPath".equals(spyProduct.getImagePath())
                && spyProduct.getQuantity() == updatedQuantity);
    }

    @Test
    public void testCreateNewProduct() {
        //Given
        ProductDto spyProductDto = Mockito.spy(ProductDto.class);
        User u = mock(User.class);
        UserDto userDto = new UserDto();
        userDto.setId(1);
        spyProductDto.setUserDto(userDto);
        //When
        Mockito.doReturn(userDto).when(spyProductDto).getUserDto();

        boolean test = underTest.createNewProduct(spyProductDto, "");
        //Then
        Assertions.assertEquals(true, test);
    }

    @Test
    @Disabled
    public void testCreateNewProductThrowsException() {
        //Given
        ProductDto spyProductDto = null;
//        when(BeanUtils.copyProperties(pDto, p)).thenThrow(IllegalAccessException.class);
//        when(underTest.createNewProduct(pDto, "")).thenThrow(IllegalAccessException.class);
//        Mockito.doThrow(IllegalAccessException.class).when(BeanUtils.copyProperties(pDto, p));
        //When
        boolean test = underTest.createNewProduct(spyProductDto, "");
        //Then
        Assertions.assertEquals(false, test);
    }

    @Test
    public void testGetDao() {
        //Given

        //When
        //Then
        Assertions.assertEquals(dao, underTest.getDao());
    }

    @Test
    public void testPurchaseProductWithOneInactiveAndOneReducedQuantityOfProduct() {
        //Given
        List<ProductDto> cart = new ArrayList<>();
        ProductDto allSoldSpyProductDto = Mockito.spy(ProductDto.class);
        allSoldSpyProductDto.setActive(1);
        allSoldSpyProductDto.setQuantity(1);
        allSoldSpyProductDto.setId(0);
        ProductDto someSoldSpyProductDto = Mockito.spy(ProductDto.class);
        someSoldSpyProductDto.setActive(1);
        someSoldSpyProductDto.setQuantity(1);
        someSoldSpyProductDto.setId(1);
        cart.add(allSoldSpyProductDto);
        cart.add(someSoldSpyProductDto);

        Product allSoldSpyProduct = Mockito.spy(Product.class);
        allSoldSpyProduct.setActive(1);
        allSoldSpyProduct.setQuantity(1);
        allSoldSpyProduct.setId(0);
        Product someSoldSpyProduct = Mockito.spy(Product.class);
        someSoldSpyProduct.setActive(1);
        someSoldSpyProduct.setQuantity(2);
        someSoldSpyProduct.setId(1);
        //When
        doReturn(Optional.of(allSoldSpyProduct)).when(dao).findById(0);
        doReturn(Optional.of(someSoldSpyProduct)).when(dao).findById(1);
        underTest.purchaseProduct(cart);

        //Then
        Assertions.assertTrue(allSoldSpyProduct.getActive() == 0
                && someSoldSpyProduct.getQuantity() == 1
                && someSoldSpyProduct.getActive() == 1);
    }

    @Test
    public void testPurchaseProductWithNoSuchProductException() {
        //Given
        List<ProductDto> cart = new ArrayList<>();
        ProductDto allSoldSpyProductDto = Mockito.spy(ProductDto.class);
        allSoldSpyProductDto.setActive(1);
        allSoldSpyProductDto.setQuantity(1);
        allSoldSpyProductDto.setId(0);
        cart.add(allSoldSpyProductDto);

        //When
        when(dao.findById(0)).thenReturn(Optional.empty());
        //Then
        Assertions.assertThrows(NoSuchProductException.class, () -> underTest.purchaseProduct(cart));
    }

    @Test
    public void testGetActiveProductsWithOneItem() throws IllegalAccessException, InvocationTargetException {
        //Given
        List<ProductDto> result = new ArrayList<>();
        User u = mock(User.class);
        Product spyProduct = Mockito.spy(Product.class);
        spyProduct.setUser(u);
        spyProduct.setActive(1);
        ProductDto dto = new ProductDto();
        BeanUtils.copyProperties(dto, spyProduct);
        UserDto uDto = mock(UserDto.class);
        dto.setUserDto(uDto);
        result.add(dto);
        when(dao.findAllActive()).thenReturn(Arrays.asList(spyProduct));
        //When
        List<ProductDto> products = underTest.getActiveProducts();

        //Then
        Assertions.assertEquals(result, products);
    }

    @Test
    public void testFilter() throws IllegalAccessException, InvocationTargetException {
        //Given
        List<Product> products = new ArrayList<>();
        Product spyProduct = Mockito.spy(Product.class);
        spyProduct.setName("testName");
        Subcategory spySubcategory = Mockito.spy(Subcategory.class);
        spyProduct.setSubcategory(spySubcategory);
        Category spyCategory = Mockito.spy(Category.class);
        spyCategory.setId(0);
        spySubcategory.setCategory(spyCategory);
        spyProduct.setImagePath("testImgPath");
        spyProduct.setDescription("testDescription");
        spyProduct.setPrice(666);
        spyProduct.setActive(1);
        spyProduct.setQuantity(1);
        User spyUser = Mockito.spy(User.class);
        spyUser.setId(0);
        spyUser.setCity("testCity");
        spyUser.setUserName("testUserName");
        spyProduct.setUser(spyUser);
        products.add(spyProduct);
        ProductDto dto = new ProductDto();
        BeanUtils.copyProperties(dto, spyProduct);
        List<ProductDto> productDtos = new ArrayList<>();
        productDtos.add(dto);
        //When
        doReturn(products).when(dao).filter("testName", 0, null, null, null, 0, 0, 0, null, null);
        List<ProductDto> result = underTest.filter("testName", 0, null, null, null, 0, 0, 0, null, null);
        //Then
        Assertions.assertEquals(productDtos, result);
    }
}
