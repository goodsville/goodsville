/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.model.services;

import hu.braininghub.bh11.goodsville.dto.UserDto;
import hu.braininghub.bh11.goodsville.dto.exceptions.NoSuchUserException;
import hu.braininghub.bh11.goodsville.repository.UserDao;
import hu.braininghub.bh11.repository.entity.User;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.beanutils.BeanUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author andra
 */
@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserDao dao;

    private UserService underTest;

    @BeforeEach
    void init() {
        underTest = new UserService();
        underTest.setDao(dao);
    }

    @Test
    public void testGetUsersWithEmptyList() {
        //Given
        when(dao.findAll()).thenReturn(new ArrayList<>());

        //When
        List<UserDto> users = underTest.getUsers();

        //Then
        Assertions.assertEquals(new ArrayList<>(), users);

    }

    @Test
    public void testGetUsersWithOneItem() throws IllegalAccessException, InvocationTargetException {
        //Given
        List<UserDto> result = new ArrayList<>();
        User u = mock(User.class);
        UserDto dto = new UserDto();

        when(dao.findAll()).thenReturn(Arrays.asList(u));

        BeanUtils.copyProperties(dto, u);
        result.add(dto);

        //When
        List<UserDto> users = underTest.getUsers();

        //Then
        Assertions.assertEquals(result, users);
    }

    @Test
    public void testGetUsersWithTwoItem() {
        //Given
        List<UserDto> result = new ArrayList<>();
        User u1 = mock(User.class);
        User u2 = mock(User.class);

        when(dao.findAll()).thenReturn(Arrays.asList(u1, u2));

        for (User u : dao.findAll()) {
            UserDto dto = new UserDto();
            try {
                BeanUtils.copyProperties(dto, u);
                result.add(dto);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(UserServiceTest.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(UserServiceTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //When
        List<UserDto> users = underTest.getUsers();
        //Then
        Assertions.assertEquals(result, users);
    }

    @Test
    public void testGetUserById() throws InvocationTargetException, InvocationTargetException, IllegalAccessException, NoSuchUserException {

        //Given
        User u = mock(User.class);
        UserDto dto = new UserDto();
        //When
        when(dao.findById(u.getId())).thenReturn(Optional.of(u));

        try {
            BeanUtils.copyProperties(dto, dao.findById(u.getId()).orElseThrow(NoSuchUserException::new));

        } catch (IllegalAccessException ex) {
            Logger.getLogger(UserServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(UserServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        UserDto user = underTest.getUserById(u.getId());
        //Then
        Assertions.assertEquals(dto, user);
    }

    @Test
    public void testGetUserByIdWithNonExistingIdNoSuchUserException() throws InvocationTargetException, InvocationTargetException, IllegalAccessException, NoSuchUserException {

        //Given
        User u = mock(User.class);
        u.setId(3);

        UserDto dto = new UserDto();
        //When
        when(dao.findById(u.getId())).thenReturn(Optional.of(u));

        try {
            BeanUtils.copyProperties(dto, dao.findById(u.getId()).orElseThrow(NoSuchUserException::new));

        } catch (IllegalAccessException ex) {
            Logger.getLogger(UserServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(UserServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Then
        Assertions.assertThrows(NoSuchUserException.class, () -> underTest.getUserById(111));
    }

    @Test
    public void testGetUsersByNameBasedOnFirstNameWithEmptyList() throws IllegalAccessException, InvocationTargetException {
        //Given
        when(dao.findByName("a")).thenReturn(new ArrayList<>());

        //When
        Iterable<UserDto> users = new ArrayList<>();
        users = underTest.getUsersByName("a");

        //Then
        Assertions.assertEquals(new ArrayList<>(), users);

    }

    @Test
    public void testGetUsersByNameBasedOnFirstNameWithOneItem() throws IllegalAccessException, InvocationTargetException {
        //Given
        User u = mock(User.class);
        List<UserDto> resultDtoList = new ArrayList<>();

        //When
        when(dao.findByName(u.getFirstName())).thenReturn(Arrays.asList(u));
        List<User> foundUsers = dao.findByName(u.getFirstName());

        foundUsers.forEach(
                (fus) -> {
                    try {
                        UserDto dto = new UserDto();
                        BeanUtils.copyProperties(dto, fus);
                        resultDtoList.add(dto);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(UserServiceTest.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InvocationTargetException ex) {
                        Logger.getLogger(UserServiceTest.class.getName()).log(Level.SEVERE, null, ex);
                    }

                });

        Iterable<UserDto> userList = new ArrayList<>();
        userList = underTest.getUsersByName(u.getFirstName());

        //Then
        Assertions.assertEquals(resultDtoList, userList);

    }

    @Test
    public void testGetUsersByNameBasedOnFirstNameWithTwoItems() throws IllegalAccessException, InvocationTargetException {
        //Given
        User u = mock(User.class);
        User u2 = mock(User.class);
        List<UserDto> resultDtoList = new ArrayList<>();

        //When
        when(dao.findByName(u.getFirstName())).thenReturn(Arrays.asList(u, u2));
        List<User> foundUsers = dao.findByName(u.getFirstName());

        foundUsers.forEach(
                (fus) -> {
                    try {
                        UserDto dto = new UserDto();
                        BeanUtils.copyProperties(dto, fus);
                        resultDtoList.add(dto);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(UserServiceTest.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InvocationTargetException ex) {
                        Logger.getLogger(UserServiceTest.class.getName()).log(Level.SEVERE, null, ex);
                    }

                });

        Iterable<UserDto> userList = new ArrayList<>();
        userList = underTest.getUsersByName(u.getFirstName());

        //Then
        Assertions.assertEquals(resultDtoList, userList);

    }

    @Test
    public void testGetUserByUserName() throws IllegalAccessException, InvocationTargetException {
        //Given
        User u = mock(User.class);
        UserDto dto = new UserDto();

        //When
        when(dao.findByUserName(u.getUserName())).thenReturn(Optional.of(u));

        BeanUtils.copyProperties(dto, dao.findByUserName(u.getUserName()).orElseThrow(NoSuchUserException::new));
        UserDto user = underTest.getUserByUserName(u.getUserName());

        //Then
        Assertions.assertEquals(dto, user);

    }

    @Test
    public void testGetUserByUserNameWithNonExistingUserNameNoSuchUserException() throws IllegalAccessException, InvocationTargetException {
        //Given
       // User u = mock(User.class);
       /// u.setUserName("validUserName");
      //  UserDto dto = new UserDto();

        //When
       // when(dao.findByUserName(u.getUserName())).thenReturn(Optional.of(u));
        when(dao.findByUserName("notExistingUserName")).thenReturn(Optional.empty());

        //    BeanUtils.copyProperties(dto, dao.findByUserName(u.getUserName()).orElseThrow(NoSuchUserException::new));
    

        //Then
        Assertions.assertThrows(NoSuchUserException.class, () -> underTest.getUserByUserName("notExistingUserName"));

    }

    //todo
    @Test
    public void testCreateNewUserWithAllData() {}
    
}
